package com.kyle.notification.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;

import static com.kyle.notification.enums.EffectFlag._PRE_EFFECT;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@ApiModel("模板信息请求")
@Data
public class TemplateInfoRequest {
    /**
     * 模板名称
     */
    @ApiModelProperty(value = "模板名称", required = true)
    @NotBlank(message = "模板名称不能为空")
    private String name;

    /**
     * 模板类型
     * {@link com.kyle.notification.enums.TemplateMode}
     */
    @ApiModelProperty(value = "模板类型", required = true)
    @NotBlank(message = "模板类型不能为空")
    private String mode;

    /**
     * 模板内容
     */
    @ApiModelProperty(value = "模板内容", required = true)
    @NotBlank(message = "模板内容不能为空")
    private String template;

    /**
     * 生效标记(0: 失效; 1: 生效; 2: 预生效)
     * {@link com.kyle.notification.enums.EffectFlag}
     */
    @ApiModelProperty(value = "生效标记")
    private Integer effectFlag = _PRE_EFFECT.getKey();
}
