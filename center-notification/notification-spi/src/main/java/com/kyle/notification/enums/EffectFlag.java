package com.kyle.notification.enums;

import com.kyle.common.enumi.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@AllArgsConstructor
public enum EffectFlag implements IEnum<Integer, String> {
    /**
     * 生效状态
     */
    _UN_EFFECT(0, "失效"),
    _EFFECTED(1, "生效"),
    _PRE_EFFECT(2, "预生效")
;

    @Getter
    private final Integer key;
    @Getter
    private final String value;
}
