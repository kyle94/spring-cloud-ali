package com.kyle.notification.service;

import com.kyle.notification.dto.NotificationInfoDTO;
import com.kyle.notification.dto.TemplateInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@Api(tags = "通知服务 -- 对外")
public interface NotificationOutService {


    /**
     * 创建模板
     * @param dto 模板请求
     */
    @PostMapping("/template-info")
    @ApiOperation("创建模板")
    void createTemplate(@RequestBody TemplateInfoDTO dto);

    /**
     * 发送通知
     * @param dto 通知请求
     */
    @PostMapping
    @ApiOperation("发送通知")
    void sendNotification(@RequestBody NotificationInfoDTO dto);
}
