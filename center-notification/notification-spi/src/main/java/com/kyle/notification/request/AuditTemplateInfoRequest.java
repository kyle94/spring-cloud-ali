package com.kyle.notification.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@ApiModel("审核模板信息请求")
@Data
public class AuditTemplateInfoRequest {
    /**
     * 模板ID
     */
    @ApiModelProperty(value = "模板ID", required = true)
    @NotBlank(message = "模板ID不能为空")
    private Long id;

    /**
     * 模板状态
     */
    @ApiModelProperty(value = "模板状态", required = true)
    @NotNull(message = "模板状态不能为空")
    private Boolean isEffect;
}
