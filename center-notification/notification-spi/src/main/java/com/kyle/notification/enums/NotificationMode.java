package com.kyle.notification.enums;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
public enum NotificationMode {
    /**
     * 通知类型
     */
    _WEB_SOCKET,
    _SMS,
    _MQ,
    _STATION_LETTER
}
