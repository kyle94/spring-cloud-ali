package com.kyle.notification.dto;

import com.kyle.notification.enums.NotificationMode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@ApiModel("通知信息请求")
@Data
public class NotificationInfoDTO {
    /**
     * 通知发起地
     * 例如：
     * 短信通知：YU-PIAN
     * websocket通知：local ip
     * 消息中间件通知：发起服务名
     * ...
     */
    @ApiModelProperty(value = "通知发起地", required = true)
    @NotBlank(message = "通知发起地不能为空")
    private String fromDir;

    /**
     * 通知接收地
     * 例如：
     * 短信通知：phone
     * websocket通知：sessionId，群发时该字段为空
     * 消息中间件通知：topic
     * ...
     */
    @ApiModelProperty(value = "通知接收地")
    private String toDir;

    /**
     * 通知接收分组
     * 例如：
     * 短信通知：phones
     * websocket通知：群发(null)，按照类型发送(clazzPre)，单发(null，当to字段必填)
     * 消息中间件通知：group
     * ...
     */
    @ApiModelProperty(value = "通知接收分组")
    private String toGroup;

    /**
     * 通知类型
     * 例如_WEB_SOCKET, _SMS, _MQ, _STATION_LETTER
     * {@link NotificationMode}
     */
    @ApiModelProperty(value = "通知类型")
    private String mode;

    /**
     * 数据实体
     * 该数据实体需继承自{@link NotificationDataDTO}
     */
    @ApiModelProperty(value = "数据实体")
    private NotificationDataDTO notificationData;

    /**
     * 模板名称
     */
    @ApiModelProperty(value = "模板名称")
    private String templateName;
}
