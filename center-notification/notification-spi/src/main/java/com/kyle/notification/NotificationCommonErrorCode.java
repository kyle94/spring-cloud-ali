package com.kyle.notification;

import com.kyle.unified.dispose.core.error.BaseErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zouxiaobang
 * @date 2021/7/13
 */
@AllArgsConstructor
public enum NotificationCommonErrorCode implements BaseErrorCode {
    /**
     * 通知相关错误信息
     */
    _TEMPLATE_NOT_FOUND(2000, "模板未找到未找到"),
    _TEMPLATE_NAME_NOT_NULL(2001, "模板名称不能为空"),
    _TEMPLATE_NOT_EFFECT(2002, "模板未生效")

    ;

    @Getter
    private final Integer code;

    @Getter
    private final String message;
}
