package com.kyle.notification.service;

import com.kyle.common.tools.asserts.AssertUtil;
import com.kyle.notification.NotificationCommonErrorCode;
import com.kyle.notification.enums.EffectFlag;
import com.kyle.notification.pojo.entity.TemplateInfo;
import com.kyle.notification.pojo.repository.TemplateInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@Slf4j
@Service
public class TemplateInfoService {
    @Resource
    private TemplateInfoRepository templateInfoRepository;

    public void save(TemplateInfo templateInfo) {
        templateInfoRepository.save(templateInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    public void audit(Long id, Boolean isEffect) {
        TemplateInfo templateInfo = getById(id);
        AssertUtil.nonNull(templateInfo, NotificationCommonErrorCode._TEMPLATE_NOT_FOUND);

        templateInfo.setEffectFlag(isEffect ? EffectFlag._EFFECTED.getKey() : EffectFlag._UN_EFFECT.getKey());
        templateInfoRepository.save(templateInfo);
    }

    public TemplateInfo getById(Long id) {
        return templateInfoRepository.getById(id);
    }

    public TemplateInfo getByName(String name, String mode) {
        return templateInfoRepository.getByName(name, mode);
    }
}
