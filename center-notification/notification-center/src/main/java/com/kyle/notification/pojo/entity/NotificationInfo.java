package com.kyle.notification.pojo.entity;

import com.kyle.common.entity.BaseEntity;
import com.kyle.notification.enums.NotificationMode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "notification_info"
)
@org.hibernate.annotations.Table(appliesTo = "notification_info", comment = "通知信息表")
@Entity
@Data
public class NotificationInfo extends BaseEntity {
    /**
     * 通知发起地
     * 例如：
     * 短信通知：YU-PIAN
     * websocket通知：local ip
     * 消息中间件通知：发起服务名
     * ...
     */
    @Column(nullable = false, columnDefinition = "varchar(64) comment '通知发起地'")
    private String fromDir;

    /**
     * 通知接收地
     * 例如：
     * 短信通知：phone
     * websocket通知：sessionId，群发时该字段为空
     * 消息中间件通知：topic
     * ...
     */
    @Column(columnDefinition = "varchar(64) comment '通知接收地'")
    private String toDir;

    /**
     * 通知接收分组
     * 例如：
     * 短信通知：phones
     * websocket通知：群发(null)，按照类型发送(clazzPre)，单发(null，当to字段必填)
     * 消息中间件通知：group
     * ...
     */
    @Column(columnDefinition = "varchar(64) comment '通知接收分组'")
    private String toGroup;

    /**
     * 通知类型
     * 例如_WEB_SOCKET, _SMS, _ROCKET_MQ
     * {@link NotificationMode}
     */
    @Column(nullable = false, columnDefinition = "varchar(16) comment '通知类型'")
    private String mode;

    /**
     * 通知内容
     */
    @Basic(fetch = FetchType.LAZY)
    @Lob
    @Column(columnDefinition = "text comment '通知内容'")
    private String notificationContent;

    /**
     * 预留字段，
     * 后面可为各种情况扩充通知内容
     */
    @Column(columnDefinition = "varchar(256) comment '预留字段'")
    private String notificationParam;
}
