package com.kyle.notification.rest;

import com.kyle.common.tools.bean.BeanCopyUtils;
import com.kyle.notification.config.SenderServiceLoader;
import com.kyle.notification.dto.NotificationInfoDTO;
import com.kyle.notification.dto.TemplateInfoDTO;
import com.kyle.notification.pojo.entity.TemplateInfo;
import com.kyle.notification.service.NotificationOutService;
import com.kyle.notification.service.TemplateInfoService;
import com.kyle.notification.service.sender.SenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@Slf4j
@RestController
public class NotificationOutServiceImpl implements NotificationOutService {
    @Resource
    private TemplateInfoService templateInfoService;

    @Resource
    private SenderServiceLoader senderServiceLoader;


    @Override
    public void createTemplate(TemplateInfoDTO dto) {
        TemplateInfo templateInfo = new TemplateInfo();
        BeanCopyUtils.copyProperties(dto, templateInfo);
        templateInfoService.save(templateInfo);
    }

    @Override
    public void sendNotification(@RequestBody NotificationInfoDTO dto) {
        SenderService senderService = senderServiceLoader.getSenderService(dto.getMode());
        senderService.send(dto);
    }
}
