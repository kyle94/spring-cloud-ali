package com.kyle.notification.annotation;

import com.kyle.notification.enums.NotificationMode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Sender {
    NotificationMode name();
}
