package com.kyle.notification.pojo.entity;

import com.kyle.common.entity.BaseEntity;
import com.kyle.notification.enums.NotificationMode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import static com.kyle.notification.enums.EffectFlag._PRE_EFFECT;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "template_info",
        uniqueConstraints = {@UniqueConstraint(columnNames = "name")}
)
@org.hibernate.annotations.Table(appliesTo = "template_info", comment = "模板信息表")
@Entity
@Data
public class TemplateInfo extends BaseEntity {
    /**
     * 模板名称
     */
    @Column(nullable = false, columnDefinition = "varchar(16) comment '模板名称'")
    private String name;

    /**
     * 模板类型
     * {@link com.kyle.notification.enums.TemplateMode}
     */
    @Column(nullable = false, columnDefinition = "varchar(16) comment '模板类型'")
    private String mode;

    /**
     * 模板内容
     */
    @Column(nullable = false, columnDefinition = "varchar(256) comment '模板内容'")
    private String template;

    /**
     * 生效标记(0: 失效; 1: 生效; 2: 预生效)
     * {@link com.kyle.notification.enums.EffectFlag}
     */
    @Column(nullable = false, columnDefinition = "tinyint unsigned comment '生效标记(0: 失效; 1: 生效; 2: 预生效)'")
    private Integer effectFlag = _PRE_EFFECT.getKey();
}
