package com.kyle.notification.service.sender;

import com.kyle.common.tools.asserts.AssertUtil;
import com.kyle.notification.NotificationCommonErrorCode;
import com.kyle.notification.annotation.Sender;
import com.kyle.notification.dto.NotificationInfoDTO;
import com.kyle.notification.enums.EffectFlag;
import com.kyle.notification.enums.NotificationMode;
import com.kyle.notification.enums.TemplateMode;
import com.kyle.notification.pojo.entity.TemplateInfo;
import com.kyle.notification.service.TemplateInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@Slf4j
@Component
@Sender(name = NotificationMode._SMS)
public class SmsSenderServiceImpl implements SenderService {
    @Resource
    private TemplateInfoService templateInfoService;

    @Override
    public void send(NotificationInfoDTO dto) {
        String templateName = dto.getNotificationParam();
        AssertUtil.nonBlank(templateName, NotificationCommonErrorCode._TEMPLATE_NAME_NOT_NULL);

        TemplateInfo templateInfo = templateInfoService.getByName(templateName, TemplateMode._SMS.name());
        AssertUtil.nonEquals(templateInfo.getEffectFlag(), EffectFlag._EFFECTED.getKey(), NotificationCommonErrorCode._TEMPLATE_NOT_EFFECT);

        // todo

    }
}
