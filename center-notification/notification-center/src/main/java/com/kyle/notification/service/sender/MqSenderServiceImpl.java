package com.kyle.notification.service.sender;

import com.kyle.notification.annotation.Sender;
import com.kyle.notification.dto.NotificationInfoDTO;
import com.kyle.notification.enums.NotificationMode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */

@Slf4j
@Component
@Sender(name = NotificationMode._MQ)
public class MqSenderServiceImpl implements SenderService {
    @Override
    public void send(NotificationInfoDTO dto) {

    }
}
