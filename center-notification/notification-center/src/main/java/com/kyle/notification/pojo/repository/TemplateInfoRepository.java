package com.kyle.notification.pojo.repository;

import com.kyle.notification.pojo.entity.TemplateInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
public interface TemplateInfoRepository extends JpaRepository<TemplateInfo, Long> {
    @Query("SELECT t FROM TemplateInfo t WHERE t.id = ?1 AND t.deleted = 0")
    TemplateInfo getById(Long id);

    @Query("SELECT t FROM TemplateInfo t WHERE t.name = ?1 AND t.mode = ?2 AND t.deleted = 0")
    TemplateInfo getByName(String name, String mode);
}
