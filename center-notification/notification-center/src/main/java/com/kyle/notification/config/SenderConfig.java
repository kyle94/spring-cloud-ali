package com.kyle.notification.config;

import com.kyle.notification.annotation.Sender;
import com.kyle.notification.enums.NotificationMode;
import com.kyle.notification.service.sender.SenderService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@Configuration
public class SenderConfig {
    @Resource
    private ApplicationContext applicationContext;

    @Bean
    public SenderServiceLoader senderServiceLoader() {
        Map<String, Object> notificationBeanMap = applicationContext.getBeansWithAnnotation(Sender.class);
        Map<String, SenderService> senderServiceMap = new ConcurrentHashMap<>();

        for (Object obj : notificationBeanMap.values()) {
            if (obj instanceof SenderService) {
                SenderService senderService = (SenderService) obj;
                Sender[] annotationsByType = senderService.getClass().getAnnotationsByType(Sender.class);
                if (annotationsByType.length > 0) {
                    // 如果一个类中有多个@Sender注解，那么默认取第一个
                    NotificationMode notificationMode = annotationsByType[0].name();
                    senderServiceMap.put(notificationMode.name(), senderService);
                }
            }
        }

        return new SenderServiceLoader(senderServiceMap);
    }
}
