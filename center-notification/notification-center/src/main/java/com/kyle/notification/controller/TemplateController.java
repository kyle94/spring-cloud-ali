package com.kyle.notification.controller;

import com.kyle.common.tools.bean.BeanCopyUtils;
import com.kyle.notification.enums.EffectFlag;
import com.kyle.notification.pojo.entity.TemplateInfo;
import com.kyle.notification.request.AuditTemplateInfoRequest;
import com.kyle.notification.request.TemplateInfoRequest;
import com.kyle.notification.service.TemplateInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
@RestController
@RequestMapping("/template")
@Api(tags = "模板相关")
public class TemplateController {
    @Resource
    private TemplateInfoService templateInfoService;

    @PostMapping
    @ApiOperation("创建模板信息")
    public void createTemplateInfo(@RequestBody TemplateInfoRequest request) {
        TemplateInfo templateInfo = new TemplateInfo();
        BeanCopyUtils.copyProperties(request, templateInfo);
        if (!Objects.equals(EffectFlag._PRE_EFFECT.getKey(), templateInfo.getEffectFlag())) {
            // 由Notification admin平台创建的，默认生效
            templateInfo.setEffectFlag(EffectFlag._EFFECTED.getKey());
        }

        templateInfoService.save(templateInfo);
    }

    @PutMapping
    @ApiOperation("审核模板信息")
    public void auditTemplateInfo(@RequestBody AuditTemplateInfoRequest request) {
        templateInfoService.audit(request.getId(), request.getIsEffect());
    }

}
