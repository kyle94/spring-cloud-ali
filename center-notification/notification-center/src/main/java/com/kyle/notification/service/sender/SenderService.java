package com.kyle.notification.service.sender;

import com.kyle.notification.dto.NotificationInfoDTO;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
public interface SenderService {
    void send(NotificationInfoDTO dto);
}
