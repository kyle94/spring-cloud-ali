package com.kyle.notification.config;

import com.kyle.notification.service.sender.SenderService;

import java.util.Map;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
public class SenderServiceLoader {
    /**
     * key: @Sender.name()
     * value: SenderService
     */
    private final Map<String, SenderService> senderServiceMap;

    public SenderServiceLoader(Map<String, SenderService> senderServiceMap) {
        this.senderServiceMap = senderServiceMap;
    }

    public Map<String, SenderService> getSenderServiceMap() {
        return senderServiceMap;
    }

    public SenderService getSenderService(String senderName) {
        return getSenderServiceMap().get(senderName);
    }
}
