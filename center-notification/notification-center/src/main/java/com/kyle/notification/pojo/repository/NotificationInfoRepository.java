package com.kyle.notification.pojo.repository;

import com.kyle.notification.pojo.entity.NotificationInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
public interface NotificationInfoRepository extends JpaRepository<NotificationInfo, Long> {
}
