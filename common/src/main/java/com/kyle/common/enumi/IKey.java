package com.kyle.common.enumi;

public interface IKey<T> {
    /**
     * 名称 KEY
     */
    T getKey();
}
