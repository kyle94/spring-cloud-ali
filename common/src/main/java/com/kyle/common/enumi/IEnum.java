package com.kyle.common.enumi;

public interface IEnum<K, V> extends IKey<K>, IValue<V> {
}
