package com.kyle.common.enumi;

public interface IValue<T> {
    /**
     * 值 VALUES
     */
    T getValue();
}
