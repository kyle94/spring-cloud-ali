package com.kyle.common.config.web.impl;

import com.google.gson.Gson;
import com.kyle.common.cache.ICacheService;
import com.kyle.common.config.web.RepeatSubmitInterceptor;
import com.kyle.common.config.web.RequestCacheData;
import com.kyle.common.config.web.filter.RepeatedlyRequestWrapper;
import com.kyle.common.tools.http.HttpParamsUtil;
import com.kyle.common.tools.str.StringUtil;
import com.kyle.common.tools.time.TimeUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
@Component
public class DefaultRepeatSubmitInterceptor extends RepeatSubmitInterceptor {
    /**
     * 请求间隔时间，单位为秒
     * 即两次请求在多少秒之内重复请求则判定为重复提交
     *
     * 默认：5s
     */
    @Value("${kyle.request.intervalTime:5}")
    private long intervalTime;

    /**
     * 请求头中可以设置该请求的唯一标识
     * 如果该唯一标识为空，则使用该请求的url。
     *
     * 解决问题：在一种情景下，使用url作为唯一标识并不合适
     * -- 当有多个客户端发起相同的请求且相同的参数，希望他们能各自完成，不算是重复提交时。
     * 如果上述情况也算重复提交时，则可以不设置repeatSubmitHeader。
     */
    @Value("${kyle.request.repeatSubmit.key:banRepeatId}")
    private String repeatSubmitHeader;

    @Resource
    private ICacheService<String> cacheService;

    public void setIntervalTime(long intervalTime) {
        this.intervalTime = intervalTime;
    }

    /**
     * 判定条件：请求url和请求参数和上次请求一致，且两次提交在间隔时间呢，则判定为重复提交
     */
    @Override
    public boolean isRepeatSubmit(HttpServletRequest request) {
        String nowParams = "";

        if (request instanceof RepeatedlyRequestWrapper) {
            RepeatedlyRequestWrapper repeatedlyRequest = (RepeatedlyRequestWrapper) request;
            nowParams = HttpParamsUtil.getBodyString(repeatedlyRequest);
        }

        // 请求的body为空
        if (StringUtils.isEmpty(nowParams)) {
            nowParams = new Gson().toJson(request.getParameterMap());
        }
        RequestCacheData nowRequestCacheData = RequestCacheData.builder()
                .url(request.getRequestURI())
                .params(nowParams)
                .time(System.currentTimeMillis())
                .build();

        // 根据repeatSubmitHeader获取请求的唯一ID
        String requestId = request.getHeader(repeatSubmitHeader);
        requestId = StringUtils.isEmpty(requestId) ? request.getRequestURI() : requestId;
        String cacheKey = ICacheService.REPEAT_SUBMIT_KEY + requestId;

        String lastRequestCacheData = cacheService.get(cacheKey);
        if (!StringUtils.isEmpty(lastRequestCacheData)) {
            RequestCacheData oldRequestCacheData = new Gson().fromJson(lastRequestCacheData, RequestCacheData.class);
            if (nowParams.equals(oldRequestCacheData.getParams())
                    && nowRequestCacheData.getUrl().equals(oldRequestCacheData.getUrl())
                    && TimeUtil.compareTimes(nowRequestCacheData.getTime(), oldRequestCacheData.getTime(), intervalTime * 1000)) {
                return true;
            }
        }

        String requestCacheData = new Gson().toJson(nowRequestCacheData);
        cacheService.put(cacheKey, requestCacheData, intervalTime, TimeUnit.SECONDS);

        return false;
    }
}
