package com.kyle.common.config.web;

import com.kyle.common.annotation.RepeatSubmit;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
@Component
public abstract class RepeatSubmitInterceptor extends HandlerInterceptorAdapter {
    /**
     * 是否重复提交请求
     * 规则由子类实现
     */
    public abstract boolean isRepeatSubmit(HttpServletRequest request);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            RepeatSubmit repeatSubmit = method.getAnnotation(RepeatSubmit.class);
            if (repeatSubmit != null && !repeatSubmit.canRepeat()) {
                // 不允许重复提交
                if (isRepeatSubmit(request)) {
                    throw new ServiceException(CommonErrorCode._REQUEST_REPEAT_SUBMIT);
                }
            }

            return true;
        }

        return super.preHandle(request, response, handler);
    }
}
