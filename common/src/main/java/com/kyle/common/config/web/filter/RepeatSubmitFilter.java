package com.kyle.common.config.web.filter;

import com.kyle.common.tools.str.StringUtil;
import org.springframework.http.MediaType;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option Web 请求拦截器，默认Order为最低级别
 */
public class RepeatSubmitFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // ignore
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && StringUtil.equalsAndIgnoreCase(request.getContentType(),
                MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE)) {
            request = new RepeatedlyRequestWrapper((HttpServletRequest) request, response);
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // ignore
    }
}
