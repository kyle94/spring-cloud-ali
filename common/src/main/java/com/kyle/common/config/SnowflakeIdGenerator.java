package com.kyle.common.config;

import com.kyle.common.tools.ids.IdGenerator;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Properties;

/**
 * @author zouxiaobang
 * @date 2021/7/16
 */
@Service
@Transactional
public class SnowflakeIdGenerator implements IdentifierGenerator, Configurable {
    @Override
    public void configure(Type type, Properties properties, ServiceRegistry serviceRegistry) throws MappingException {

    }

    @Override
    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o) throws HibernateException {
        return IdGenerator.snowflakeId();
    }
}
