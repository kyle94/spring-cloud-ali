package com.kyle.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zouxiaobang
 * @date 2021/7/16
 */
@Configurable
@Data
@ConfigurationProperties(prefix = "snowflake")
public class SnowflakeProperties {
    /**
     * 工作机器ID
     */
    private String workerId;

    /**
     * 数据中心ID
     */
    private String dataCenterId;
}
