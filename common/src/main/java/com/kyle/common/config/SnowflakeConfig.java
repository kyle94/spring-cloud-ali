package com.kyle.common.config;

import com.kyle.common.tools.ids.SnowFlowIdHelper;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;

/**
 * @author zouxiaobang
 * @date 2021/7/16
 */
@Configurable
public class SnowflakeConfig {
    @Resource
    private SnowflakeProperties snowflakeProperties;

    @Bean
    public SnowFlowIdHelper snowFlowIdHelper() {
        return new SnowFlowIdHelper(Long.parseLong(snowflakeProperties.getWorkerId()),
                Long.parseLong(snowflakeProperties.getDataCenterId()));
    }
}
