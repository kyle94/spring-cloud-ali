package com.kyle.common.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @author xb.zou
 * @date 2020/9/22
 * @option
 */
@Slf4j
@Aspect
@Component
public class LoggerAspect {
    @Pointcut("execution(public * com.kyle..*Controller.*(..))")
    public void log(){}

    @Before("log()")
    public void doBefore(JoinPoint joinPoint) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        log.info("【URL】: {}", request.getRequestURL());
        log.info("【METHOD】: {}", request.getMethod());
        log.info("【IP】: {}", request.getRemoteAddr());
        log.info("【CLASS_METHOD】: {}",
                joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        log.info("【ARGS】: {}", Arrays.toString(joinPoint.getArgs()));

    }

    @AfterReturning(returning = "returnValue", pointcut = "log()")
    public void doAfter(Object returnValue) {
        log.info("【响应参数】：{}", returnValue);
    }
}
