package com.kyle.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${spring.application.name}")
    private String appName;
    @Value("${swagger.version: v1.0.0}")
    private String version;
    @Value("${swagger.name.cn: 阿里cloud案例}")
    private String appCnName;
    @Value("${swagger.params.package:}")
    private String basePackage;

    /**
     * swagger.params.headers=Authorization,用户认证,false;X-Code,采用技术
     */
    @Value("${swagger.params.headers:}")
    private String headers;


    @Bean
    public Docket createRestApi() {
        // test
        log.info("【Swagger】name: {}", appName);
        log.info("【Swagger】version: {}", version);
        log.info("【Swagger】CnName: {}", appCnName);
        log.info("【Swagger】basePackage: {}", basePackage);
        log.info("【Swagger】headers: {}", headers);

        // 设置Swagger需要显示请求头: Authorization
        List<Parameter> parameters = resolveHeaders(headers);

        if (parameters == null) {
            return new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .select()
                    .apis(RequestHandlerSelectors.basePackage(basePackage))
                    .paths(PathSelectors.any())
                    .build();
        }

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .globalOperationParameters(parameters)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(appName)
                .description(appCnName)
                .version(version)
                .build();
    }

    private List<Parameter> resolveHeaders(String headersContent) {
        if (StringUtils.isEmpty(headersContent)) {
            return null;
        }

        String[] headerParams = headersContent.split(";");

        List<Parameter> headers = new ArrayList<>();

        for (String headerParam: headerParams) {
            String[] params = headerParam.split(",");
            if (params.length < 2) {
                throw new RuntimeException("【Swagger】Swagger配置异常：请正确填写application.swagger.headers配置参数");
            }

            String paramName = params[0];
            String paramDescription = params[1];
            boolean paramRequired = params.length > 2 && Boolean.parseBoolean(params[2]);

            Parameter parameter = new ParameterBuilder()
                    .name(paramName)
                    .description(paramDescription)
                    .modelRef(new ModelRef("string"))
                    .parameterType("header")
                    .required(paramRequired)
                    .build();

            headers.add(parameter);
        }

        return headers;
    }
}
