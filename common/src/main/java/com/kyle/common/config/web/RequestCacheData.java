package com.kyle.common.config.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestCacheData {
    private String url;
    private String params;
    private long time;
}
