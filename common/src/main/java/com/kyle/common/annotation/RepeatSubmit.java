package com.kyle.common.annotation;

import java.lang.annotation.*;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option 防止重复提交请求的注解
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {
    /**
     * 是否允许重复提交
     * 默认不允许
     */
    boolean canRepeat() default false;
}
