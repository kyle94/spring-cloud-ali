package com.kyle.common.tools.ids;

import com.kyle.common.tools.bean.BeanContextUtils;

/**
 * @author zouxiaobang
 * @date 2021/7/16
 */
public class IdGenerator {
    public static long snowflakeId() {
        return BeanContextUtils.getBean(SnowFlowIdHelper.class).nextId();
    }
}
