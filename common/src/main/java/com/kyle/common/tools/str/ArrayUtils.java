package com.kyle.common.tools.str;

import java.lang.reflect.Array;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
public class ArrayUtils {
    public static <T> boolean isNotEmpty(T[] array) {
        return !isEmpty(array);
    }

    public static <T> boolean isEmpty(T[] array) {
        return getLength(array) == 0;
    }

    public static <T> int getLength(T[] array) {
        return array == null ? 0 : Array.getLength(array);
    }
}
