package com.kyle.common.tools.ids;

import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;

/**
 * @author zouxiaobang
 * @date 2021/7/13
 * 0(1bit) | 00000000000000000000000000000000000000000(41bit) | 0000000000(10bit) | 000000000000(12bit)
 */
public class SnowFlowIdHelper {
    /**
     * 初始时间戳: 2015-01-01
     */
    private final long START_TIME_STAMP = 1420041600000L;

    /**
     * 序列号ID位数 -- 12位
     */
    private static final long SEQUENCE_BITS = 12L;

    /**
     * 工作机器ID位数 -- 5位
     */
    private static final long WORKER_ID_BITS = 5L;

    /**
     * 数据中心ID位数 -- 5位
     */
    private static final long DATA_CENTER_ID_BITS = 5L;

    /**
     * 时间戳位数 -- 41位
     */
    private static final long TIMESTAMP_BITS = 41L;

    /**
     * 工作机器ID 最大值
     */
    private static final long MAX_WORKER_ID = ~(-1 << WORKER_ID_BITS);

    /**
     * 数据中心ID 最大值
     */
    private static final long MAX_DATA_CENTER_ID = ~(-1 << DATA_CENTER_ID_BITS);

    /**
     * 序列号掩码 -- 补位(0x111111111111)
     */
    private static final long SEQUENCE_MASK = ~(-1L << SEQUENCE_BITS);

    /**
     * 工作机器ID
     */
    private final long workerId;

    /**
     * 数据中心ID
     */
    private final long dataCenterId;

    /**
     * 序列号
     */
    private long sequence;

    /**
     * 上一个时间戳
     */
    private static long lastTimeStamp;

    public SnowFlowIdHelper(long workerId, long dataCenterId) {
        if (workerId > MAX_WORKER_ID || workerId < 0) {
            throw new ServiceException(CommonErrorCode._ID_TO_LARGE.getCode(),
                    String.format("工作机器ID过大或小于0: %d", workerId));
        }

        if (dataCenterId > MAX_DATA_CENTER_ID || dataCenterId < 0) {
            throw new ServiceException(CommonErrorCode._ID_TO_LARGE.getCode(),
                    String.format("数据中心ID过大或小于0: %d", workerId));
        }

        this.workerId = workerId;
        this.dataCenterId = dataCenterId;
    }

    public synchronized long nextId() {
        long timeStamp = generateTime();
        if (timeStamp < lastTimeStamp) {
            // 时钟回摆
            throw new ServiceException(CommonErrorCode._CLOCK_BACK);
        }

        if (timeStamp == lastTimeStamp) {
            doSameTimeStamp();
        } else {
            sequence = 0L;
        }

        return buildId();
    }

    private long buildId() {
        return ((lastTimeStamp - START_TIME_STAMP) << (SEQUENCE_BITS + WORKER_ID_BITS + DATA_CENTER_ID_BITS))
                | (dataCenterId << (SEQUENCE_BITS + WORKER_ID_BITS))
                | (workerId << SEQUENCE_BITS)
                | sequence;
    }

    /**
     * 生成ID在同一毫秒内
     */
    private void doSameTimeStamp() {
        sequence = (sequence + 1) & SEQUENCE_MASK;
        // 溢出, 阻塞等下下一个毫秒
        if (sequence == 0) {
            lastTimeStamp = blockToNextMillis();
        }
    }

    private long blockToNextMillis() {
        long timeStamp = generateTime();
        while (timeStamp <= lastTimeStamp) {
            timeStamp = generateTime();
        }

        return timeStamp;
    }

    private static long generateTime() {
        return System.currentTimeMillis();
    }
}
