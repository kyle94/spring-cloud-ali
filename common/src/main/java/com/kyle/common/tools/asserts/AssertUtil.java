package com.kyle.common.tools.asserts;

import com.kyle.unified.dispose.core.error.BaseErrorCode;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
public class AssertUtil {
    public static void nonNull(Object obj, BaseErrorCode errors) {
        if (Objects.isNull(obj)) {
            throw new ServiceException(errors);
        }
    }

    public static void nonNull(Object obj, String errorMsg) {
        if (Objects.isNull(obj)) {
            throw new ServiceException(CommonErrorCode._REQUEST_ERROR.getCode(), errorMsg);
        }
    }

    public static void nonBlank(String str, BaseErrorCode errors) {
        if (StringUtils.isEmpty(str)) {
            throw new ServiceException(errors);
        }
    }

    public static void nonBlank(String str, String errorMsg) {
        if (StringUtils.isEmpty(str)) {
            throw new ServiceException(CommonErrorCode._REQUEST_ERROR.getCode(), errorMsg);
        }
    }

    public static void nonEquals(Object obj1, Object obj2, BaseErrorCode errors) {
        if (!Objects.equals(obj1, obj2)) {
            throw new ServiceException(errors);
        }
    }

    public static void nonEquals(Object obj1, Object obj2, String errorMsg) {
        if (!Objects.equals(obj1, obj2)) {
            throw new ServiceException(CommonErrorCode._REQUEST_ERROR.getCode(), errorMsg);
        }
    }
}
