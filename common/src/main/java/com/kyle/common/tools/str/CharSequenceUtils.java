package com.kyle.common.tools.str;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
public class CharSequenceUtils {
    public static boolean regionMatches(CharSequence charSequence, boolean ignoreCase,
                                        int thisStart, CharSequence subString, int start, int length) {
        if (charSequence instanceof String && subString instanceof String) {
            return ((String) charSequence).regionMatches(ignoreCase, thisStart, (String) subString, start, length);
        }

        int indexThisStart = thisStart;
        int indexStart = start;
        int tempLength = length;

        final int srcLength = charSequence.length() - thisStart;
        final int subLength = subString.length() - start;

        if (thisStart < 0 || start < 0 || length < 0) {
            return false;
        }

        if (srcLength < length || subLength < length) {
            return false;
        }

        while (tempLength-- > 0) {
            char srcChar = charSequence.charAt(indexThisStart++);
            char subChar = subString.charAt(indexStart++);

            if (srcChar == subChar) {
                continue;
            }

            if (!ignoreCase) {
                return false;
            }

            // 判断在忽略大小写的情况下是否相等
            if (Character.toUpperCase(srcChar) != Character.toUpperCase(subChar)
                    && Character.toLowerCase(srcChar) != Character.toLowerCase(subChar)) {
                return false;
            }
        }

        return true;
    }
}
