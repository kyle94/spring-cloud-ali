package com.kyle.common.tools.time;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
public class TimeUtil {
    public static long gapOfTimes(long time1, long time2) {
        return time1 - time2;
    }

    public static boolean compareTimes(long time1, long time2, long gap) {
        return Math.abs(gapOfTimes(time1, time2)) < gap;
    }
}
