package com.kyle.common.tools.type;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
public class SimpleTypeUtil {
    public static boolean isSimpleType(Class<?> typeClass) {
        return typeClass == Integer.class
                || typeClass == Long.class
                || typeClass == Short.class
                || typeClass == Boolean.class
                || typeClass == Byte.class
                || typeClass == Character.class
                || typeClass == Double.class
                || typeClass == Float.class
                || typeClass == String.class;
    }

    public static Class<?> parseSimpleType(Object obj) {
        if (obj instanceof Integer) {
            return Integer.class;
        } else if (obj instanceof Long) {
            return Long.class;
        } else if (obj instanceof Short) {
            return Short.class;
        } else if (obj instanceof Boolean) {
            return Boolean.class;
        } else if (obj instanceof Byte) {
            return Byte.class;
        } else if (obj instanceof Character) {
            return Character.class;
        } else if (obj instanceof Double) {
            return Double.class;
        } else if (obj instanceof Float) {
            return Float.class;
        } else if (obj instanceof String) {
            return String.class;
        } else {
            return Object.class;
        }
    }
}
