package com.kyle.common.tools.file;


import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
public class FileUtil {
    /**
     * 将内容写入到文件总，并且返回新创建的所有文件以及目录
     */
    public static List<File> writeToFile(File file, String data, String encoding) {
        return writeToFile(file, data, encoding, false);
    }

    private static List<File> writeToFile(File file, String data, String encoding, boolean append) {

        List<File> allFiles = new ArrayList<>();
        if (file.exists()) {
            if (file.isDirectory()) {
                throw new ServiceException(CommonErrorCode._FILE_IS_DIRECTORY);
            }

            if (!file.canWrite()) {
                throw new ServiceException(CommonErrorCode._FILE_CAN_NOT_WRITE);
            }
        } else {
            allFiles.add(file);
            // 创建目录
            File parentFile = file.getParentFile();
            if (parentFile != null) {
                boolean isMkdirsSuccess = false;
                if (!parentFile.exists()) {
                    isMkdirsSuccess = parentFile.mkdirs();
                    allFiles.add(parentFile);
                }

                if (!isMkdirsSuccess && !parentFile.isDirectory()) {
                    throw new ServiceException(CommonErrorCode._FILE_PATH_CAN_NOT_CREATED);
                }
            }
        }

        try(FileOutputStream outputStream = new FileOutputStream(file, append)) {
            if (!StringUtils.isEmpty(data)) {
                outputStream.write(data.getBytes(Charset.forName(encoding)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return allFiles;
    }

    /**
     * 删除文件
     */
    public static void deleteFiles(List<File> files) {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
    }
}
