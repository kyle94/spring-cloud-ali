package com.kyle.common.tools.bean;

import com.kyle.common.tools.asserts.AssertUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author zouxiaobang
 * @date 2021/6/10
 */
public class BeanCopyUtils extends BeanUtils {
    public static void copyPropertiesDeeply(Object source, Object target) {
        copyProperties(source, target, null, null);
    }

    private static void copyProperties(Object source, Object target, @Nullable Class<?> editable,
                                       @Nullable String... ignoreProperties) throws BeansException {
        AssertUtil.nonNull(source, "Source must not be null");
        AssertUtil.nonNull(target, "Target must not be null");

        Class<?> actualEditable = target.getClass();
        if (editable != null) {
            if (!editable.isInstance(target)) {
                throw new IllegalArgumentException("Target class [" + target.getClass().getName() +
                        "] not assignable to Editable class [" + editable.getName() + "]");
            }
            actualEditable = editable;
        }
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        List<String> ignoreList = (ignoreProperties != null ? Arrays.asList(ignoreProperties) : null);

        for (PropertyDescriptor targetPd : targetPds) {
            Method writeMethod = targetPd.getWriteMethod();
            if (writeMethod != null && (ignoreList == null || !ignoreList.contains(targetPd.getName()))) {
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (sourcePd != null) {
                    Method readMethod = sourcePd.getReadMethod();

                    if (readMethod != null) {
                        setAccessible(readMethod);
                        setAccessible(writeMethod);
                        // 1、筛选出名字相同，但是类型不同的方法
                        Class<?> writeParamType = writeMethod.getParameterTypes()[0];
                        Class<?> readReturnType = readMethod.getReturnType();
                        boolean isClassSame = ClassUtils.isAssignable(writeParamType, readReturnType);
                        if (!isClassSame) {
                            // 2、判断类型是否SimpleName相同，但是类型不相同
                            if (writeParamType.getSimpleName().equals(readReturnType.getSimpleName())) {
                                // 3、实例化该属性
                                Object instance = null;
                                try {
                                    instance = writeParamType.getDeclaredConstructor().newInstance();
                                    writeMethod.invoke(target, instance);
                                    // 4、递归调用
                                    Object innerObj = readMethod.invoke(source);
                                    if (Objects.nonNull(innerObj)) {
                                        copyPropertiesDeeply(innerObj, targetPd.getReadMethod().invoke(target));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            try {
                                Object value = readMethod.invoke(source);
                                writeMethod.invoke(target, value);
                            } catch (Throwable ex) {
                                throw new FatalBeanException(
                                        "Could not copy property '" + targetPd.getName() + "' from source to target", ex);
                            }
                        }

                    }
                }
            }
        }
    }

    private static void setAccessible(Method method) {
        if (!Modifier.isPublic(method.getDeclaringClass().getModifiers())) {
            method.setAccessible(true);
        }
    }
}
