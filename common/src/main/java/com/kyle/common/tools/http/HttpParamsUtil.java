package com.kyle.common.tools.http;

import lombok.extern.slf4j.Slf4j;
import javax.servlet.ServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
@Slf4j
public class HttpParamsUtil {
    public static String getBodyString(ServletRequest request) {
        StringBuilder sb = new StringBuilder();

        try (InputStream inputStream = request.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))){
            String line = "";

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            log.error("【HttpParamsUtil.getBodyString】报错: {}", e.getMessage());
        }

        return sb.toString();
    }
}
