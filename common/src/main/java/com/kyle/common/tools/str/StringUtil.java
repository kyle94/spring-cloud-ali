package com.kyle.common.tools.str;


import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xb.zou
 * @date 2020/9/25
 * @option
 */
public class StringUtil extends StringUtils {
    private static final Pattern HUMP_PATTERN = Pattern.compile("[A-Z]");

    public static boolean equalsAndIgnoreCase(CharSequence originStr, CharSequence... wantStr) {
        if (ArrayUtils.isNotEmpty(wantStr)) {
            for (CharSequence charSequence : wantStr) {
                if (equalsIgnoreCase(originStr, charSequence)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static boolean equalsIgnoreCase(CharSequence originStr, CharSequence charSequence) {
        if (originStr != null && charSequence != null) {
            if (originStr == charSequence) {
                return true;
            } else {
                return originStr.length() == charSequence.length()
                        && CharSequenceUtils.regionMatches(originStr, true, 0, charSequence, 0, originStr.length());
            }
        }

        return originStr == charSequence;
    }

    /**
     * 将驼峰转换为_
     */
    public static String humpToLine(String source) {
        Matcher matcher = HUMP_PATTERN.matcher(source);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }

        matcher.appendTail(sb);
        return sb.toString();
    }
}
