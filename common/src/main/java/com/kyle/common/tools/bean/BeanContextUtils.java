package com.kyle.common.tools.bean;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zouxiaobang
 * @date 2021/7/16
 */
@Component
public class BeanContextUtils implements ApplicationContextAware {
    private static ApplicationContext context;

    @SuppressWarnings("all")
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BeanContextUtils.context = applicationContext;
    }

    public static Object getBean(String beanName) {
        return context.getBean(beanName);
    }

    public static <T> T getBean(Class<T> clazz) {
        return context.getBean(clazz);

    }

    public static <T> Map<String, T> getBeansByType(Class<T> clazz) {

        return context.getBeansOfType(clazz);
    }
}
