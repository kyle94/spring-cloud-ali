package com.kyle.common.tools.file;

import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author xb.zou
 * @date 2020/9/27
 * @option
 */
public class ZipUtil {
    public static byte[] writeToZip(List<File> files) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream)) {
            for (File file : files) {
                // 将文件添加到zip中
                ZipEntry zipEntry = new ZipEntry(file.getPath());
                zipEntry.setSize(file.length());
                zipOutputStream.putNextEntry(zipEntry);

                // 写入内容
                InputStream inputStream = new FileInputStream(file);
                byte[] buffer = new byte[1024];
                int length = 0;
                while ((length = inputStream.read(buffer)) != -1) {
                    zipOutputStream.write(buffer, 0, length);
                }
                inputStream.close();

                zipOutputStream.flush();
                zipOutputStream.closeEntry();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteArrayOutputStream.toByteArray();
    }
}
