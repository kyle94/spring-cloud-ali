package com.kyle.common.cache;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public interface ICacheService<T> {
    String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 写入
     */
    void put(String key, T value);
    void put(String key, T value, Long timeout, TimeUnit timeUnit);
    void put(Map<String, T> map);

    /**
     * 读出
     */
    T get(String key);
    T getAndRemove(String key);
    Long getExpire(String key, TimeUnit timeUnit);
    Map<String, T> getAll(List<String> keys);
    List<T> scanPreWord(String preWord);

    /**
     * 删除
     */
    void remove(String key);

    /**
     * 检查
     */
    boolean hasKey(String key);
}
