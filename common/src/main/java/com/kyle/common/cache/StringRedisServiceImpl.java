package com.kyle.common.cache;

import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class StringRedisServiceImpl implements ICacheService<String> {
    @Resource
    private StringRedisTemplate template;

    @Override
    public void put(String key, String value) {
        remove(key);

        template.opsForValue().set(key, value);
    }

    @Override
    public void put(String key, String value, Long timeout, TimeUnit timeUnit) {
        remove(key);

        template.opsForValue().set(key, value, timeout, timeUnit);
    }

    @Override
    public void put(Map<String, String> map) {
        template.executePipelined((RedisCallback<String>) connection -> {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                connection.set(entry.getKey().getBytes(), entry.getValue().getBytes());
            }

            return null;
        });
    }

    @Override
    public String get(String key) {
        return template.opsForValue().get(key);
    }

    @Override
    public String getAndRemove(String key) {
        String value = template.opsForValue().get(key);
        template.delete(key);
        return value;
    }

    @Override
    public Long getExpire(String key, TimeUnit timeUnit) {
        return template.getExpire(key, timeUnit);
    }

    @Override
    public Map<String, String> getAll(List<String> keys) {
        Map<String, String> result = new HashMap<>(keys.size());

        List<Object> objects = template.executePipelined((RedisCallback<String>) redisConnection -> {
            StringRedisConnection stringRedisConnection = (StringRedisConnection) redisConnection;
            for (String key : keys) {
                stringRedisConnection.get(key);
            }

            return null;
        });
        List<String> values = objects.stream().map(String::valueOf).collect(Collectors.toList());

        for (int i = 0; i < keys.size(); i++) {
            result.put(keys.get(i), values.get(i));
        }

        return result;
    }

    @Override
    public List<String> scanPreWord(String preWord) {
        List<String> keys = new ArrayList<>();

        template.execute((RedisConnection connection) -> {
            try (Cursor<byte[]> cursor = connection.scan(ScanOptions.scanOptions().count(1000).match(preWord + "*").build())) {
                cursor.forEachRemaining(item -> {
                    // 匹配到的key
                    String key = new String(item, StandardCharsets.UTF_8);
                    keys.add(key);
                });
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });

        return keys;
    }

    @Override
    public void remove(String key) {
        if (hasKey(key)) {
            template.delete(key);
        }
    }

    @Override
    public boolean hasKey(String key) {
        return template.hasKey(key);
    }
}
