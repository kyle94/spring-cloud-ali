package com.kyle.common.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author xb.zou
 * @date 2020/9/22
 * @option
 */
@MappedSuperclass
@Data
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = -8231013904330367793L;

    @Id
    @GeneratedValue(generator = "snowflakeIdGenerator")
    @GenericGenerator(name = "snowflakeIdGenerator", strategy = "com.kyle.common.config.SnowflakeIdGenerator")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "gmt_create", updatable = false)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.CreationTimestamp
    private Date gmtCreate;

    @Column(name = "gmt_modified")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @org.hibernate.annotations.UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date gmtModified;

    /** 1删除， 0未删除 */
    @Column(columnDefinition = "tinyint unsigned", name = "is_deleted")
    private Integer deleted = 0;
}
