package com.kyle.content.controller;

import com.kyle.common.annotation.RepeatSubmit;
import com.kyle.common.cache.ICacheService;
import com.kyle.generate.pojo.GenerateData;
import com.kyle.generate.service.IGenerateService;
import com.kyle.unified.dispose.core.error.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/test")
@Api(tags = "测试")
@RefreshScope
public class TestController {
    @Resource
    private ICacheService<String> stringCacheService;
    @Resource
    private IGenerateService generateService;

    @Value("${config.test.code: 400}")
    private Integer code;
    @Value("${config.test.msg: 获取失败}")
    private String msg;
    @Value("${config.test.security.msg: 获取失败}")
    private String securityMsg;


    @ApiOperation("获取远程配置 -- 需要权限")
    @GetMapping("/security/config")
    @PreAuthorize("hasAnyAuthority('system:config:list')")
    @RepeatSubmit
    public String getConfig() {
        return "security msg from nacos is " + securityMsg;
    }

    @ApiOperation("获取远程配置")
    @GetMapping("/config")
    @RepeatSubmit
    public String getConfigWithoutSecurity() {
        return "返回结果: code from nacos is " + code + ", msg from nacos is " + msg;
    }
    @PostMapping("/redis")
    @ApiOperation("测试Redis")
    public void testPutRedis(@RequestParam String key, @RequestParam String value) {
        stringCacheService.put(key, value);
    }

    @GetMapping("/redis")
    @ApiOperation("测试Redis")
    public String testRedis(@RequestParam String key) {
        return stringCacheService.get(key);
    }

    @GetMapping("/preview/code")
    @ApiOperation("自动生成代码测试")
    public Map<String, GenerateData> testPreviewCode(@RequestParam(required = false) String author,
                                                             @RequestParam(required = false) String pkgName,
                                                             @RequestParam(required = false) String domainClassPkg,
                                                             @RequestParam(required = false) String domainClassName) {
        return generateService.previewCode(author, pkgName, domainClassPkg, domainClassName);
    }

    @PutMapping("/generate/code")
    @ApiOperation("自动生成代码测试")
    public void testGenerateCode(@RequestParam(required = false) String author,
                                         @RequestParam(required = false) String pkgName,
                                         @RequestParam(required = false) String domainClassPkg,
                                         @RequestParam(required = false) String domainClassName,
                                         @RequestParam(required = false) String moduleName,
                                         HttpServletResponse response) throws IOException {
        byte[] data = generateService.generateCodeToZip(author, pkgName, domainClassPkg, domainClassName, moduleName);
        response.reset();
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setHeader("Content-Disposition", "attachment; filename=\"code.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        response.getOutputStream().write(data);
    }


    @GetMapping("/exception")
    public void testException() {
        throw new ServiceException(40213, "测试错误");
    }
}
