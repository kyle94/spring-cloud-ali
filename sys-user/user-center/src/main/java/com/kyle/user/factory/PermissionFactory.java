package com.kyle.user.factory;

import com.kyle.user.dto.MenuDTO;
import com.kyle.user.dto.PermissionDTO;
import com.kyle.user.response.MenuResponse;
import com.kyle.user.response.PermissionResponse;
import com.kyle.user.pojo.entities.Menu;
import com.kyle.user.pojo.entities.Permission;
import org.springframework.beans.BeanUtils;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
public class PermissionFactory {
    public static PermissionDTO create(Permission permission) {
        Menu menu = permission.getMenu();
        MenuDTO menuDTO = MenuFactory.create(menu);
        PermissionDTO permissionDTO = new PermissionDTO();
        BeanUtils.copyProperties(permission, permissionDTO);
        permissionDTO.setMenu(menuDTO);

        return permissionDTO;
    }
}
