package com.kyle.user.pojo.entities;

import com.kyle.common.entity.BaseEntity;
import com.kyle.user.enumi.LockStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Table(
        name = "role",
        uniqueConstraints = {@UniqueConstraint(columnNames = "role_name_en")}
)
@org.hibernate.annotations.Table(appliesTo = "role", comment = "角色表")
@Data
@Entity
public class Role extends BaseEntity {
    /**
     * 角色名称 -- 英文
     */
    @Column(name = "role_name_en", nullable = false, columnDefinition = "varchar(32) comment '角色名称 -- 英文'")
    private String roleNameEn;

    /**
     * 角色名称 -- 中文
     */
    @Column(name = "role_name_cn", nullable = false, columnDefinition = "varchar(32) comment '角色名称 -- 中文'")
    private String roleNameCn;

    /**
     * 菜单排序，排序越小，权限越大
     */
    @Column(name = "order_num", nullable = false, columnDefinition = "int comment '菜单排序'")
    private Integer orderNum;

    /**
     * 创建者
     */
    @Column(name = "created_by", nullable = false, columnDefinition = "varchar(64) comment '创建者'")
    private String createdBy;

    /**
     * 更新者
     */
    @Column(name = "updated_by", columnDefinition = "varchar(64) comment '更新者'")
    private String updatedBy;

    /**
     * 锁定标记
     * {@link LockStatus}
     */
    @Column(name = "lock_flag", nullable = false, columnDefinition = "tinyint(2) comment '锁定标记(-1：已删除；0：状态正常；1：已冻结)'")
    private Integer lockFlag = LockStatus._STATUS_NORMAL.getKey();

    /**
     * 角色权限关联表
     */
    @ManyToMany
    @JoinTable(
            name = "role_permission",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id")
    )
    private Set<Permission> permissions;
}
