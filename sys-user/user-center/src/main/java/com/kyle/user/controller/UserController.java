package com.kyle.user.controller;

import com.kyle.common.tools.asserts.AssertUtil;
import com.kyle.common.tools.bean.BeanCopyUtils;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.user.UserCommonErrorCode;
import com.kyle.user.dto.UserDTO;
import com.kyle.user.pojo.entities.UserInfo;
import com.kyle.user.response.SimpleUserInfoResponse;
import com.kyle.user.response.UserInfoResponse;
import com.kyle.user.service.UserOutService;
import com.kyle.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
@Api(tags = "用户相关")
public class UserController {
    @Resource
    private UserService userService;

    @Resource
    private UserOutService userOutService;


    @GetMapping("/by/name")
    @ApiOperation("通过名称获取用户信息")
    public UserInfoResponse get(@RequestParam String userName) {
        UserDTO userInfo = userOutService.getUserInfo(userName);

        UserInfoResponse userInfoResponse = new UserInfoResponse();
        BeanCopyUtils.copyPropertiesDeeply(userInfo, userInfoResponse);

        return userInfoResponse;
    }

    @GetMapping("/simple/by/name")
    @ApiOperation("通过名称获取用户的简要信息")
    public SimpleUserInfoResponse getSimple(@RequestParam String userName) {
        UserInfo userInfo = userService.getByName(userName);
        AssertUtil.nonNull(userInfo, UserCommonErrorCode._USER_NOT_FOUND);

        SimpleUserInfoResponse simpleUserInfoResponse = new SimpleUserInfoResponse();
        BeanCopyUtils.copyProperties(userInfo, simpleUserInfoResponse);

        return simpleUserInfoResponse;
    }

    @DeleteMapping
    @ApiOperation("删除用户")
    public void deleteUser(@RequestParam String userId) {
        // 权限判断
        // 被删除用户所在的部门
        // 删除审批

    }
}
