package com.kyle.user.service;

import com.kyle.user.pojo.entities.UserInfo;
import com.kyle.user.pojo.repositories.UserInfoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
@Slf4j
@Service
public class UserService {
    @Resource
    private UserInfoRepository userInfoRepository;

    public UserInfo getByName(String userName) {
        return userInfoRepository.getByName(userName);
    }
}
