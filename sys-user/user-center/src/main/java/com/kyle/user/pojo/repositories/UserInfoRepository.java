package com.kyle.user.pojo.repositories;

import com.kyle.user.pojo.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author xb.zou
 * @date 2020/9/22
 * @option
 */
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {

    @Query("SELECT u FROM UserInfo u WHERE u.deleted=0 AND u.name=?1")
    UserInfo getByName(String userName);
}
