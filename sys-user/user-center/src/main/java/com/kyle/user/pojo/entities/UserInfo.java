package com.kyle.user.pojo.entities;

import com.kyle.common.entity.BaseEntity;
import com.kyle.user.enumi.LockStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

/**
 * @author xb.zou
 * @date 2020/9/22
 * @option
 */
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "user_info",
        uniqueConstraints = {@UniqueConstraint(columnNames = "mobile")}
)
@org.hibernate.annotations.Table(appliesTo = "user_info", comment = "用户信息表")
@Entity
@Data
public class UserInfo extends BaseEntity {
    /**
     * 用户名
     */
    @Column(nullable = false, columnDefinition = "varchar(64) comment '用户名'")
    private String name;

    /**
     * 密码
     */
    @Column(nullable = false, columnDefinition = "varchar(64) comment '密码'")
    private String password;

    /**
     * 盐
     */
    @Column(columnDefinition = "varchar(64) comment '密码加盐'")
    private String salt;

    /**
     * 锁定标记
     * {@link LockStatus}
     */
    @Column(name = "lock_flag", nullable = false, columnDefinition = "tinyint(2) comment '锁定标记(-1：已删除；0：状态正常；1：已冻结)'")
    private Integer lockFlag = LockStatus._STATUS_NORMAL.getKey();

    /**
     * 手机号
     */
    @Column(columnDefinition = "varchar(32) comment '手机号'")
    private String mobile;

    /**
     * 头像
     */
    @Column(columnDefinition = "varchar(255) comment '头像'")
    private String avatar;

    /**
     * 用户权限关联表
     */
    @ManyToMany
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;
}
