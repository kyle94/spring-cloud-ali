package com.kyle.user.controller.account;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zouxiaobang
 * @date 2021/6/19
 */
@RestController
@RequestMapping("/register")
@Api(tags = "注册相关")
public class RegisterController {

}
