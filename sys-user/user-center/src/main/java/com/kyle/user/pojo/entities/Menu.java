package com.kyle.user.pojo.entities;

import com.kyle.common.entity.BaseEntity;
import com.kyle.user.enumi.LockStatus;
import com.kyle.user.enumi.MenuTypes;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author xb.zou
 * @date 2020/9/23
 * @option
 */
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "menu",
        uniqueConstraints = {@UniqueConstraint(columnNames = "code")}
)
@org.hibernate.annotations.Table(appliesTo = "menu", comment = "前端目录表")
@Data
@Entity
public class Menu extends BaseEntity {
    /**
     * 菜单名称
     */
    @Column(nullable = false, columnDefinition = "varchar(64) comment '菜单名称'")
    private String name;

    /**
     * 菜单编码
     * 父菜单编码为当前菜单编码取模100： code % 100
     * 如果code < 100，则为一级菜单，不需要计算其父菜单
     */
    @Column(nullable = false, columnDefinition = "bigint(64) comment '菜单编码'")
    private Long code;

    /**
     * 菜单显示顺序：在同级菜单中排序
     */
    @Column(name = "order_num", nullable = false, columnDefinition = "int comment '菜单显示顺序'")
    private Integer orderNum;

    /**
     * 路由地址
     */
    @Column(name = "route_path", columnDefinition = "varchar(128) comment '路由地址'")
    private String routePath;

    /**
     * 组件地址
     */
    @Column(name = "component_path", columnDefinition = "varchar(128) comment '组件地址'")
    private String componentPath;

    /**
     * 是否外链
     */
    @Column(name = "is_enternal_link", columnDefinition = "tinyint unsigned comment '是否外链'")
    private Integer isExternalLink = 0;

    /**
     * 是否缓存
     */
    @Column(name = "is_cache", columnDefinition = "tinyint unsigned comment '是否缓存'")
    private Integer isCache = 1;

    /**
     * 菜单类型，默认为菜单类型
     * D: 目录；M：菜单；B：按钮
     * {@link com.kyle.user.enumi.MenuTypes}
     */
    @Column(name = "menu_type", nullable = false, columnDefinition = "varchar(8) comment '菜单类型，默认为菜单类型(D: 目录；M：菜单；B：按钮)'")
    private String menuType = MenuTypes._TYPE_MENU.getKey();

    /**
     * 是否显示
     */
    @Column(name = "is_visible", nullable = false, columnDefinition = "tinyint unsigned comment '是否显示'")
    private Integer isVisible;

    /**
     * 锁定状态
     * {@link com.kyle.user.enumi.LockStatus}
     */
    @Column(name = "lock_flag", nullable = false, columnDefinition = "tinyint(2) comment '是否锁定(-1：已删除；0：状态正常；1：已冻结)'")
    private Integer lockFlag = LockStatus._STATUS_NORMAL.getKey();

    /**
     * 菜单图标
     */
    @Column(columnDefinition = "varchar(255) comment '菜单图标'")
    private String icon;

    /**
     * 创建者
     */
    @Column(name = "created_by", nullable = false, columnDefinition = "varchar(64) comment '创建者'")
    private String createdBy;

    /**
     * 更新者
     */
    @Column(name = "updated_by", columnDefinition = "varchar(64) comment '更新者'")
    private String updatedBy;
}
