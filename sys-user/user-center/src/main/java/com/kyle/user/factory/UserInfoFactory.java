package com.kyle.user.factory;

import com.kyle.user.dto.RoleDTO;
import com.kyle.user.dto.UserDTO;
import com.kyle.user.response.RoleResponse;
import com.kyle.user.response.UserInfoResponse;
import com.kyle.user.pojo.entities.Role;
import com.kyle.user.pojo.entities.UserInfo;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
public class UserInfoFactory {
    /**
     * 创建用户信息返回
     */
    public static UserDTO create(UserInfo userInfo) {
        Set<Role> roles = userInfo.getRoles();
        List<RoleDTO> roleDTOs = roles.stream().map(RoleFactory::create).collect(Collectors.toList());
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userInfo, userDTO);
        userDTO.setRoles(roleDTOs);

        return userDTO;
    }
}
