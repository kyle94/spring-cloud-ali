package com.kyle.user.rest;

import com.kyle.common.tools.asserts.AssertUtil;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.user.UserCommonErrorCode;
import com.kyle.user.dto.UserDTO;
import com.kyle.user.factory.UserInfoFactory;
import com.kyle.user.pojo.entities.UserInfo;
import com.kyle.user.service.UserOutService;
import com.kyle.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
@Slf4j
@RestController
public class UserOutServiceImpl implements UserOutService {
    @Resource
    private UserService userService;

    @Override
    public UserDTO getUserInfo(@RequestParam String userName) {
        UserInfo userInfo = userService.getByName(userName);

        AssertUtil.nonNull(userInfo, UserCommonErrorCode._USER_NOT_FOUND);

        return UserInfoFactory.create(userInfo);
    }
}
