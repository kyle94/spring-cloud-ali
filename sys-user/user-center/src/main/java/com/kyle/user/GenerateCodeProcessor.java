package com.kyle.user;

import com.kyle.generate.service.impl.GenerateServiceImpl;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
public class GenerateCodeProcessor {
    public static void main(String[] args) throws IOException {
        // 默认文件创建
//        InputStream inputStream = GenerateCodeProcessor.class.getClassLoader().getResourceAsStream("generator.yml");
        InputStream inputStream = GenerateCodeProcessor.class.getClassLoader().getResourceAsStream("generator_data.yml");
        Yaml yaml = new Yaml();
        Map<String, Map<String, String>> map = yaml.load(inputStream);
        inputStream.close();

        Map<String, String> generator = map.get("generator");
        String author = generator.get("author");
        String packageName = generator.get("packageName");
        String domainClassName = generator.get("domainClassName");
        String domainClassPkg = generator.get("domainClassPkg");
        String moduleName = generator.get("moduleName");

        // 生成文件
        GenerateServiceImpl service = new GenerateServiceImpl();
        service.generateCode(author, packageName, domainClassPkg, domainClassName, moduleName);
    }
}
