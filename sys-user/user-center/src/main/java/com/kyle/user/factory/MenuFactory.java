package com.kyle.user.factory;

import com.kyle.user.dto.MenuDTO;
import com.kyle.user.response.MenuResponse;
import com.kyle.user.pojo.entities.Menu;
import org.springframework.beans.BeanUtils;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
public class MenuFactory {
    public static MenuDTO create(Menu menu) {
        MenuDTO menuDTO = new MenuDTO();
        BeanUtils.copyProperties(menu, menuDTO);

        return menuDTO;
    }
}
