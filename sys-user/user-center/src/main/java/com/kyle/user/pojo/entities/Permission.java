package com.kyle.user.pojo.entities;

import com.kyle.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
@EqualsAndHashCode(callSuper = true)
@Table(
        name = "permission"
)
@org.hibernate.annotations.Table(appliesTo = "permission", comment = "权限表")
@Data
@Entity
public class Permission extends BaseEntity {

    /**
     * 权限标识
     */
    @Column(name = "permission_flag", nullable = false, columnDefinition = "varchar(64) comment '权限标识'")
    private String permissionFlag;

    /**
     * 权限名称
     */
    @Column(name = "name", nullable = false, columnDefinition = "varchar(64) comment '权限名称'")
    private String name;

    /**
     * 权限目录关联表
     */
    @OneToOne
    @JoinColumn(name="menu_id",referencedColumnName="id")
    private Menu menu;
}
