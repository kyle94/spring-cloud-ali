package com.kyle.user.factory;

import com.kyle.user.dto.PermissionDTO;
import com.kyle.user.dto.RoleDTO;
import com.kyle.user.response.PermissionResponse;
import com.kyle.user.response.RoleResponse;
import com.kyle.user.pojo.entities.Permission;
import com.kyle.user.pojo.entities.Role;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
public class RoleFactory {
    public static RoleDTO create(Role role) {
        Set<Permission> permissions = role.getPermissions();
        List<PermissionDTO> permissionDTOs = permissions.stream().map(PermissionFactory::create).collect(Collectors.toList());
        RoleDTO roleDTO = new RoleDTO();
        BeanUtils.copyProperties(role, roleDTO);
        roleDTO.setPermissions(permissionDTOs);

        return roleDTO;
    }
}
