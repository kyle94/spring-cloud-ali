package com.kyle.user.response;

import com.kyle.user.enumi.LockStatus;
import com.kyle.user.enumi.MenuTypes;
import lombok.Data;

import java.util.Date;

/**
 * @author xb.zou
 * @date 2020/9/24
 * @option
 */
@Data
public class MenuResponse {
    private String id;
    private Date gmtCreate;
    private Date gmtModified;
    private String name;
    private Long code;
    private Integer orderNum;
    private String routePath;
    private String componentPath;
    private Integer isExternalLink = 0;
    private Integer isCache = 1;
    private String menuType = MenuTypes._TYPE_MENU.getKey();
    private Integer isVisible;
    private Integer lockFlag = LockStatus._STATUS_NORMAL.getKey();
    private String icon;
    private String createdBy;
    private String updatedBy;
}
