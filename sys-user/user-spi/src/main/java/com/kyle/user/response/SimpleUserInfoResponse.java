package com.kyle.user.response;

import lombok.Data;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option
 */
@Data
public class SimpleUserInfoResponse {
    private String id;

    private String name;

    private String mobile;
}
