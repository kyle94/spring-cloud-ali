package com.kyle.user.response;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserInfoResponse {
    private String id;

    private Date gmtCreate;

    private Date gmtModified;

    private String name;

    private String salt;

    private Integer lockFlag;

    private String mobile;

    private String avatar;

    private List<RoleResponse> roles;
}
