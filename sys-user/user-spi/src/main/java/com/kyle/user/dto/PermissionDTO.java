package com.kyle.user.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
@Data
public class PermissionDTO {
    private String id;

    private Date gmtCreate;

    private Date gmtModified;

    /**
     * 权限标识
     */
    private String permissionFlag;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限目录关联表
     */
    private MenuDTO menu;
}
