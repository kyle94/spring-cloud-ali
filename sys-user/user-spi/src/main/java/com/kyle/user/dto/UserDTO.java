package com.kyle.user.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
@Data
public class UserDTO {
    private String id;

    private Date gmtCreate;

    private Date gmtModified;

    private String name;

    private String password;

    private String salt;

    private Integer lockFlag;

    private String mobile;

    private String avatar;

    private List<RoleDTO> roles;
}
