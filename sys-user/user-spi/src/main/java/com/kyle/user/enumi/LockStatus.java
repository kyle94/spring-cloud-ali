package com.kyle.user.enumi;

import com.kyle.common.enumi.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum LockStatus implements IEnum<Integer, String> {
    /**
     * 锁定状态
     */
    _STATUS_DELETED(-1, "已删除"),
    _STATUS_NORMAL(0, "状态正常"),
    _STATUS_LOCKED(1, "已冻结")
    ;


    @Getter
    private final Integer key;
    @Getter
    private final String value;
}
