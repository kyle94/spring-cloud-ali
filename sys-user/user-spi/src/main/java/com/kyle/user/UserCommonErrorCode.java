package com.kyle.user;

import com.kyle.unified.dispose.core.error.BaseErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zouxiaobang
 * @date 2021/7/13
 */
@AllArgsConstructor
public enum UserCommonErrorCode implements BaseErrorCode {
    /**
     * 用户错误信息
     */
    _USER_NOT_FOUND(1000, "用户未找到");

    @Getter
    private final Integer code;

    @Getter
    private final String message;
}
