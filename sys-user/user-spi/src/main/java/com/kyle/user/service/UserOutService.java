package com.kyle.user.service;

import com.kyle.user.dto.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 * @desc 用户服务（对外）
 */
@Api(tags = "用户服务 -- 对外")
public interface UserOutService {
    /**
     * 通过用户名获取用户信息
     * @param userName 用户名
     * @return 用户信息
     */
    @GetMapping("/by/name")
    @ApiOperation("根据用户名获取用户信息 -- 全量信息")
    UserDTO getUserInfo(@RequestParam String userName);
}
