package com.kyle.user;

import com.kyle.user.request.BaseSmsRequest;

/**
 * @author zouxiaobang
 * @date 2021/7/17
 */
public interface BaseSmsService<T extends BaseSmsRequest> {
    String handle(T request);
}
