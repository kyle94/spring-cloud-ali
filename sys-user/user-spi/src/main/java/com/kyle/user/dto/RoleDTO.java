package com.kyle.user.dto;

import com.kyle.user.response.PermissionResponse;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
@Data
public class RoleDTO {
    private String id;

    private Date gmtCreate;

    private Date gmtModified;

    private String roleNameEn;

    private String roleNameCn;

    private List<PermissionDTO> permissions;
}
