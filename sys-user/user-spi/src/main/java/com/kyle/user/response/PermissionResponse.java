package com.kyle.user.response;

import lombok.Data;

/**
 * @author zouxiaobang
 * @date 2021/6/21
 */
@Data
public class PermissionResponse {
    /**
     * 权限标识
     */
    private String permissionFlag;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限目录关联表
     */
    private MenuResponse menu;
}
