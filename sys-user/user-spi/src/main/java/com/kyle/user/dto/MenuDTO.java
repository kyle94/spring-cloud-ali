package com.kyle.user.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
@Data
public class MenuDTO {
    private String id;
    private Date gmtCreate;
    private Date gmtModified;
    private String name;
    private Long code;
    private Integer orderNum;
    private String routePath;
    private String componentPath;
    private Integer isExternalLink;
    private Integer isCache;
    private String menuType;
    private Integer isVisible;
    private Integer lockFlag;
    private String icon;
    private String createdBy;
    private String updatedBy;
}
