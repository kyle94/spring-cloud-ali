package com.kyle.user.response;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class RoleResponse {
    private String id;

    private Date gmtCreate;

    private Date gmtModified;

    private String roleNameEn;

    private String roleNameCn;

    private List<PermissionResponse> permissions;
}
