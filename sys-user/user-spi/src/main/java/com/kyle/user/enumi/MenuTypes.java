package com.kyle.user.enumi;

import com.kyle.common.enumi.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author xb.zou
 * @date 2020/9/24
 * @option
 */
@AllArgsConstructor
public enum MenuTypes implements IEnum<String, String> {
    /**
     * 菜单类型
     */
    _TYPE_DIRECTORY("D", "目录"),
    _TYPE_MENU("M", "菜单"),
    _TYPE_BUTTON("B", "按钮")
    ;

    @Getter
    private String key;
    @Getter
    private String value;
}
