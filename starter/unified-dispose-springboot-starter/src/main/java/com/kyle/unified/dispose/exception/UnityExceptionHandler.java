package com.kyle.unified.dispose.exception;

import com.kyle.unified.dispose.ResultUtil;
import com.kyle.unified.dispose.ResultUtilFactory;
import com.kyle.unified.dispose.annotation.IgnoreUnifiedHandler;
import com.kyle.unified.dispose.config.DisposeProperties;
import com.kyle.unified.dispose.core.BaseResult;
import com.kyle.unified.dispose.core.error.BaseErrorCode;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 统一对异常进行处理
 */
@Slf4j
@RestControllerAdvice
public class UnityExceptionHandler {
    @Resource
    private DisposeProperties disposeProperties;

    /**
     * 业务异常
     * @param e 错误信息
     * @return 异常返回结果
     */
    @ExceptionHandler(ServiceException.class)
    public BaseResult<?> handleServiceException(ServiceException e)
            throws Throwable {
        checkExceptionHandle(e);
        printError(ServiceException.class, e.getErrorCode(), e);

        return buildResult(e.getErrorCode(), e);
    }

    /**
     * 资源未找到异常
     *
     * @param e 404
     * @return 异常返回结果
     */
    @ExceptionHandler(value = NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public BaseResult<?> handleNoHandlerFoundException(NoHandlerFoundException e) throws Throwable {
        checkExceptionHandle(e);
        printWarning(NoHandlerFoundException.class, CommonErrorCode._REQUEST_NOT_FOUND, e);

        return buildResult(CommonErrorCode._REQUEST_NOT_FOUND, e);
    }

    /**
     * 请求方法错误
     * @param e 405
     * @return 异常返回结果
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public BaseResult<?> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e)
            throws Throwable {
        checkExceptionHandle(e);
        printWarning(HttpRequestMethodNotSupportedException.class, CommonErrorCode._METHOD_NOT_ALLOW, e);

        return buildResult(CommonErrorCode._METHOD_NOT_ALLOW, e);
    }

    /**
     * media-type不支持
     * @param e 415
     * @return 异常返回结果
     */
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public BaseResult<?> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e)
            throws Throwable {
        checkExceptionHandle(e);
        printWarning(HttpMediaTypeNotSupportedException.class, CommonErrorCode._UNSUPPORTED_MEDIA_TYPE, e);

        return buildResult(CommonErrorCode._UNSUPPORTED_MEDIA_TYPE, e);
    }

    /**
     * HTTP MESSAGE读取失败
     * @param e 416
     * @return 异常返回结果
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public BaseResult<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) throws Throwable {
        checkExceptionHandle(e);
        printError(HttpMessageNotReadableException.class, CommonErrorCode._REQUEST_READ_FAIL, e);

        return buildResult(CommonErrorCode._REQUEST_READ_FAIL, e);
    }

    /**
     * HTTP MESSAGE读取失败
     * @param e 526
     * @return 异常返回结果
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public BaseResult<?> handleConstraintViolationException(ConstraintViolationException e) throws Throwable {
        checkExceptionHandle(e);
        printError(HttpMessageNotReadableException.class, CommonErrorCode._CONSTRAINT_VIOLATION_ERROR, e);

        return buildResult(CommonErrorCode._CONSTRAINT_VIOLATION_ERROR, e);
    }

    /**
     * 参数校验无效
     * @param e 526
     * @return 异常返回结果
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public BaseResult<?> handleMethodArgumentNotValidException(BindException e) throws Throwable {
        checkExceptionHandle(e);
        BindingResult exceptions = e.getBindingResult();

        if (exceptions.hasErrors()) {
            List<ObjectError> errors = exceptions.getAllErrors();
            if (!CollectionUtils.isEmpty(errors)) {
                // 这里列出了全部错误参数，按正常逻辑，只需要第一条错误即可
                FieldError fieldError = (FieldError) errors.get(0);
                CommonErrorCode paramInvalidError = CommonErrorCode._PARAM_INVALID_ERROR;
                paramInvalidError.setMessage(fieldError.getDefaultMessage());
                printError(HttpMessageNotReadableException.class, paramInvalidError, e);
                return buildResult(paramInvalidError, e);
            }
        }

        printError(HttpMessageNotReadableException.class, CommonErrorCode._PARAM_INVALID_ERROR, e);
        return buildResult(CommonErrorCode._PARAM_INVALID_ERROR, e);
    }

    /**
     * Exception 类捕获 500 异常处理
     */
    @ExceptionHandler(value = Exception.class)
    public BaseResult<?> handlerException(Exception e) throws Throwable {
        checkExceptionHandle(e);

        BaseResult<?> otherException = handleOtherException(e);
        if (Objects.nonNull(otherException)) {
            return otherException;
        }

        return ifDepthExceptionType(e);
    }

    /**
     * 处理一些其他依赖或自定义的错误
     * @param e
     * @return
     */
    private BaseResult<?> handleOtherException(Exception e) {
        List<ExceptionBean> otherExceptions = disposeProperties.getOtherExceptions();
        Optional<ExceptionBean> exceptionBean = otherExceptions.stream()
                .filter(otherException -> e.getClass().getName().contains(otherException.getExceptionName()))
                .findFirst();

        if (exceptionBean.isPresent()) {
            try {
                ExceptionBean eb = exceptionBean.get();
                String errorDesc = eb.getErrorDesc();
                String[] split = errorDesc.split(":");

                return buildResult(Integer.parseInt(split[0]), split[1], e);
            } catch (Exception exception) {
                log.error("参数配置不规范: ", exception);
                return null;
            }
        }

        return null;
    }

    /**
     * 二次深度检查错误类型
     */
    private BaseResult<?> ifDepthExceptionType(Throwable throwable) throws Throwable {
        Throwable cause = throwable.getCause();
        printError(Exception.class, CommonErrorCode._SYSTEM_ERROR, throwable);
        return buildResult(CommonErrorCode._SYSTEM_ERROR, throwable);
    }



    /**
     * 构建返回的错误信息
     * @param errorCode 错误码枚举
     * @param e 异常信息
     * @return 返回的统一信息
     */
    private BaseResult<?> buildResult(BaseErrorCode errorCode, Throwable e) {
        return buildResult(errorCode.getCode(), errorCode.getMessage(), e);
    }

    /**
     * 构建返回的错误信息
     * @param e 异常信息
     * @return 返回的统一信息
     */
    private BaseResult<?> buildResult(Integer code, String message, Throwable e) {
        try {
            ResultUtil<?> resultUtil = ResultUtilFactory.create(disposeProperties);
            if (disposeProperties.isPrintExceptionMessage()) {
                return resultUtil.failure(code, e.getMessage());
            } else {
                return resultUtil.failure(code, message);
            }
        } catch (Exception ex) {
            log.error("【重要】您提供的Result类解析异常", e);
            return null;
        }
    }

    private void printError(Class<?> exceptionClass, BaseErrorCode errorCode, Throwable e) {
        log.error("[{}] ({}){}, e: ", exceptionClass.getSimpleName(), errorCode.getCode(), errorCode.getMessage(), e);
    }

    private void printWarning(Class<?> exceptionClass, BaseErrorCode errorCode, Throwable e) {
        log.warn("[{}] ({}){}, e: ", exceptionClass.getSimpleName(), errorCode.getCode(), errorCode.getMessage(), e);
    }

    /**
     * 判断是否处理异常
     *
     * @param e 异常
     */
    private <T extends Throwable> void checkExceptionHandle(T e) throws Throwable {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (Objects.isNull(requestAttributes)) {
            return;
        }

        HttpServletRequest request = requestAttributes.getRequest();
        HandlerMethod handlerMethod = (HandlerMethod) request.getAttribute(HandlerMapping.BEST_MATCHING_HANDLER_ATTRIBUTE);
        if (Objects.isNull(handlerMethod)) {
            return;
        }

        // 注解优先限制方法
        Method method = handlerMethod.getMethod();
        IgnoreUnifiedHandler methodAnnotation = method.getAnnotation(IgnoreUnifiedHandler.class);
        if (Objects.nonNull(methodAnnotation)) {
            if (!methodAnnotation.canHandleException()) {
                throw e;
            } else {
                return;
            }
        }

        // 类声明的注解做兜底
        Class<?> controllerBean = handlerMethod.getBeanType();
        IgnoreUnifiedHandler classAnnotation = controllerBean.getAnnotation(IgnoreUnifiedHandler.class);
        if (Objects.nonNull(classAnnotation)) {
            if (!classAnnotation.canHandleException()) {
                throw e;
            }
        }
    }
}
