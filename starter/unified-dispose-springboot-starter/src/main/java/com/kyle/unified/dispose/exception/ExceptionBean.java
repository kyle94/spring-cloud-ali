package com.kyle.unified.dispose.exception;

import lombok.Data;

/**
 * @author zouxiaobang
 * @date 2021/7/12
 */
@Data
public class ExceptionBean {
    private String exceptionName;
    private String errorDesc;
}
