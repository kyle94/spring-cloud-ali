package com.kyle.unified.dispose.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 统一结果处理的自动配置入口
 */
@Configuration
@EnableConfigurationProperties(DisposeProperties.class)
@PropertySource(value = "classpath:dispose.properties", encoding = "UTF-8")
public class DisposeAutoConfiguration extends WebMvcConfigurationSupport {
}
