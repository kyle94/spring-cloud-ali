package com.kyle.unified.dispose.config;

import com.alibaba.fastjson.JSON;
import com.kyle.unified.dispose.*;
import com.kyle.unified.dispose.annotation.IgnoreUnifiedHandler;
import com.kyle.unified.dispose.core.BasePageResult;
import com.kyle.unified.dispose.core.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author zouxiaobang
 * @date 2021/7/7
 */
@Slf4j
@RestControllerAdvice
public class UnityResponseAdvice implements ResponseBodyAdvice<Object> {
    private final DisposeProperties disposeProperties;

    /**
     * 构造器注入
     * @param disposeProperties 统一处理配置
     */
    public UnityResponseAdvice(DisposeProperties disposeProperties) {
        this.disposeProperties = disposeProperties;
    }

    @Override
    @SuppressWarnings("all")
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        return isSupport(methodParameter);
    }

    @Override
    @SuppressWarnings("all")
    public Object beforeBodyWrite(Object returnObj, MethodParameter methodParameter,
                                  MediaType mediaType,
                                  Class<? extends HttpMessageConverter<?>> aClass,
                                  ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse) {
        ResultUtil resultUtil;
        try {
            resultUtil = ResultUtilFactory.create(disposeProperties);
        } catch (Exception e) {
            log.error("【重要】您提供的Result类解析异常", e);
            return null;
        }

        // 设置response默认的content-type的编码为UTF-8
        MediaType contentType = serverHttpResponse.getHeaders().getContentType();

        if (Objects.isNull(serverHttpResponse.getHeaders())
                || Objects.isNull(serverHttpResponse.getHeaders().getContentType())
                || Objects.isNull(serverHttpResponse.getHeaders().getContentType().getCharset())) {
            serverHttpResponse.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        }

        if (Objects.isNull(returnObj)) {
            // 当 returnObj 返回类型为 string 并且为null会出现 java.lang.ClassCastException: Result cannot be cast to java.lang.String
            if (methodParameter.getParameterType().getName().equals("java.lang.String")) {
                return JSON.toJSON(resultUtil.success()).toString();
            }
            return resultUtil.success();
        }

        if ((returnObj instanceof BaseResult) || returnObj instanceof BasePageResult) {
            return returnObj;
        }

        // string 特殊处理 java.lang.ClassCastException: Result cannot be cast to java.lang.String
        if (returnObj instanceof String) {
            return JSON.toJSON(resultUtil.success(returnObj)).toString();
        }



        // todo 未对page进行处理
        return resultUtil.success(returnObj);
    }

    /**
     * 判断返回对象是否支持统一返回处理
     * @param methodParameter 方法返回对象
     */
    private Boolean isSupport(MethodParameter methodParameter) {
        Class<?> declaringClass = methodParameter.getDeclaringClass();

        // 该包路径不需处理
        long count = disposeProperties.getFilterPackages().stream()
                .filter(packageName -> declaringClass.getName().contains(packageName))
                .count();
        if (count > 0) {
            return false;
        }

        // 该类不需处理
        if (disposeProperties.getFilterClazz().contains(declaringClass.getName())) {
            return false;
        }

        // 采用IgnoreUnifiedHandler注解的类不需处理
        if (declaringClass.isAnnotationPresent(IgnoreUnifiedHandler.class)) {
            return false;
        }

        // 采用IgnoreUnifiedHandler注解的方法不需处理
        if (methodParameter.getMethod().isAnnotationPresent(IgnoreUnifiedHandler.class)) {
            return false;
        }

        return true;
    }
}
