package com.kyle.unified.dispose;

import com.kyle.unified.dispose.config.DisposeProperties;
import com.kyle.unified.dispose.core.BasePageResult;
import com.kyle.unified.dispose.core.BaseResult;
import com.kyle.unified.dispose.core.PageResult;
import com.kyle.unified.dispose.core.Result;
import org.springframework.util.StringUtils;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 根据配置生产出不同的ResultUtil
 */
public class ResultUtilFactory {
    private static String resultClazzName;
    private static String pageResultClazzName;
    private final static ResultUtil<?> RESULT_UTIL = new ResultUtil<>();

    public static ResultUtil<?> create(DisposeProperties disposeProperties) throws Exception {
        fetchResultUtil(disposeProperties);

        fetchPageResultUtil(disposeProperties);

        return RESULT_UTIL;
    }

    private static void fetchResultUtil(DisposeProperties disposeProperties) throws Exception {
        String resultName = getResultName(disposeProperties);
        if (StringUtils.hasLength(resultName)) {
            if (!resultName.equals(resultClazzName)) {
                resultClazzName = resultName;
            }

            Class<?> resultClazz = Class.forName(resultName);
            BaseResult result = (BaseResult) resultClazz.getDeclaredConstructor().newInstance();
            RESULT_UTIL.result(result);
        } else {
            // 采用默认Result
            RESULT_UTIL.result(new Result<>());
            resultClazzName = Result.class.getName();
        }
    }

    private static String getResultName(DisposeProperties disposeProperties) {
        // custom className -> custom class -> default
        String resultClazzName = disposeProperties.getResultClazzName();
        if (StringUtils.hasLength(resultClazzName)) {
            resultClazzName = disposeProperties.getResultClazz().getName();
        }

        return StringUtils.hasLength(resultClazzName) ? resultClazzName : Result.class.getName();
    }

    private static void fetchPageResultUtil(DisposeProperties disposeProperties) throws Exception {
        String pageResultName = getPageResultName(disposeProperties);
        if (StringUtils.hasLength(pageResultName)) {
            if (!pageResultName.equals(pageResultClazzName)) {
                Class<?> pageResultClazz = Class.forName(pageResultName);
                BasePageResult pageResult = (BasePageResult) pageResultClazz.getDeclaredConstructor().newInstance();
                RESULT_UTIL.pageResult(pageResult);

                pageResultClazzName = pageResultName;
            }
        } else {
            // 采用默认PageResult
            RESULT_UTIL.pageResult(new PageResult<>());
            pageResultClazzName = PageResult.class.getName();
        }
    }

    private static String getPageResultName(DisposeProperties disposeProperties) {
        // custom className -> custom class -> default
        String pageResultClazzName = disposeProperties.getPageResultClazzName();
        if (StringUtils.hasLength(pageResultClazzName)) {
            pageResultClazzName = disposeProperties.getPageResultClazz().getName();
        }

        return StringUtils.hasLength(pageResultClazzName) ? pageResultClazzName : PageResult.class.getName();
    }
}
