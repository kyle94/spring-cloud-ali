package com.kyle.unified.dispose.config;

import com.kyle.unified.dispose.core.BaseResult;
import com.kyle.unified.dispose.exception.ExceptionBean;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option
 */
@Slf4j
@Data
@ConfigurationProperties(DisposeProperties.PREFIX)
public class DisposeProperties {
    public static final String PREFIX = "dispose";

    /**
     * 是否要打印出异常的信息
     */
    private boolean isPrintExceptionMessage = false;

    /**
     * 过滤掉的包名，采用","隔开
     */
    private List<String> filterPackages = new ArrayList<>();

    /**
     * 过滤掉的类名，采用","隔开
     */
    private List<String> filterClazz = new ArrayList<>();

    /**
     * 返回结果数据结果类对象
     */
    private Class<?> resultClazz;

    /**
     * 返回结果数据结果类名，全限定类名
     */
    private String resultClazzName;

    /**
     * 返回分页结果数据结果类对象
     */
    private Class<?> pageResultClazz;

    /**
     * 返回分页结果数据结果类名，全限定类名
     */
    private String pageResultClazzName;

    /**
     * 其他错误
     */
    private List<ExceptionBean> otherExceptions;

    /**
     * 修改lombok设置的setter方法
     * 将类名转换为类对象
     * @param resultClazzName 全限定类名
     */
    public void setResultClazzName(String resultClazzName) throws ClassNotFoundException {
        this.resultClazzName = resultClazzName;
        this.resultClazz = Class.forName(resultClazzName);
    }

    /**
     * 返回统一结果对象
     * @return 结果对象
     */
    @SuppressWarnings("all")
    public <T> BaseResult<T> getResult() {
        try {
            return (BaseResult<T>) getResultClazz().newInstance();
        } catch (InstantiationException e) {
            log.error("[unified-dispose] can't instance: ", e);
        } catch (IllegalAccessException e) {
            log.error("[unified-dispose] can't access: ", e);
        }

        return null;
    }

    /**
     * 修改lombok设置的setter方法
     * 将类名转换为类对象
     * @param pageResultClazzName 全限定类名
     */
    public void setPageResultClazzName(String pageResultClazzName) throws ClassNotFoundException {
        this.pageResultClazzName = pageResultClazzName;
        this.pageResultClazz = Class.forName(pageResultClazzName);
    }

    /**
     * 返回统一结果对象
     * @return 结果对象
     */
    @SuppressWarnings("all")
    public <T> BaseResult<T> getPageResult() {
        try {
            return (BaseResult<T>) getResultClazz().newInstance();
        } catch (InstantiationException e) {
            log.error("[unified-dispose] can't instance: ", e);
        } catch (IllegalAccessException e) {
            log.error("[unified-dispose] can't access: ", e);
        }

        return null;
    }
}
