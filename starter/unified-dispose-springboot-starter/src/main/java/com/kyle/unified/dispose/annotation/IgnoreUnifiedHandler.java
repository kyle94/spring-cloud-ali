package com.kyle.unified.dispose.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 使用该注解决定是否忽略统一处理，可使用于方法或类声明上
 * result：只要使用该注解，则该类或方法不会返回统一格式
 * exception：默认该类或方法还是会统一处理异常，如果"canHandleException=false"，则不对异常进行处理
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreUnifiedHandler {
    /**
     * 是否对异常进行统一处理
     * @return 处理/不处理：默认处理
     */
    boolean canHandleException() default true;
}
