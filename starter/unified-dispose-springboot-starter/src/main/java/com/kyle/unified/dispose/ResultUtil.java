package com.kyle.unified.dispose;

import com.kyle.unified.dispose.core.BasePageResult;
import com.kyle.unified.dispose.core.BaseResult;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 对结果数据进行构建
 */
@Slf4j
public class ResultUtil<T> {
    private static final Integer SUCCESS_CODE = 200;
    private static final String SUCCESS_MSG = "response success";

    private BaseResult<T> result;
    private BasePageResult<T> pageResult;

    void result(BaseResult<T> result) {
        this.result = result;
    }

    void pageResult(BasePageResult<T> pageResult) {
        this.pageResult = pageResult;
    }

    public BaseResult<T> success() {
        return success(null);
    }

    public BaseResult<T> success(T data) {
        result.setCode(SUCCESS_CODE);
        result.setMsg(SUCCESS_MSG);
        result.setData(data);

        return result;
    }

    public BaseResult<T> failure(Integer code, String msg) {
        result.setCode(code);
        result.setMsg(msg);

        return result;
    }

    public BasePageResult<T> page(List<T> rows, Long total) {
        pageResult.setCode(SUCCESS_CODE);
        pageResult.setMsg(SUCCESS_MSG);
        pageResult.setData(rows);
        pageResult.setTotal(total);

        return pageResult;
    }
}
