package com.kyle.unified.dispose.core.error;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 错误码的数据结构接口定义
 * 原则上不允许code和message拥有setter方法
 */
public interface BaseErrorCode {
    /**
     * 自定义异常编码
     * @return 异常编码
     */
    Integer getCode();

    /**
     * 自定义异常信息
     * @return 异常信息
     */
    String getMessage();
}
