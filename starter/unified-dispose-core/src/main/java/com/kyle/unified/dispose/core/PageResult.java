package com.kyle.unified.dispose.core;

import lombok.Data;

import java.util.List;

/**
 * @author zouxiaobang
 * @date 2021/4/27
 */
@Data
public class PageResult<T> implements BasePageResult<T> {
    /**
     * 总记录数
     */
    private Long total;

    private Integer code;

    private String msg;

    private List<T> data;
}
