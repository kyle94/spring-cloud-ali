package com.kyle.unified.dispose.core.error;

import lombok.Getter;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 自定义异常，该异常只识别运行时异常
 */
public class ServiceException extends RuntimeException {
    @Getter
    private final Integer code;
    @Getter
    private final BaseErrorCode errorCode;

    public ServiceException(Integer code, String message) {
        super(message);

        this.code = code;
        this.errorCode = buildErrorCode(code, message);
    }

    public ServiceException(BaseErrorCode errorCode) {
        super(errorCode.getMessage());

        this.code = errorCode.getCode();
        this.errorCode = errorCode;
    }

    public ServiceException(BaseErrorCode errorCode, String supplement) {
        super(errorCode.getMessage() + ": " + supplement);

        this.code = errorCode.getCode();
       this.errorCode = buildErrorCode(code, errorCode.getMessage() + ": " + supplement);
    }

    private BaseErrorCode buildErrorCode(Integer code, String message) {
        return new BaseErrorCode() {
            @Override
            public Integer getCode() {
                return code;
            }

            @Override
            public String getMessage() {
                return message;
            }
        };
    }
}
