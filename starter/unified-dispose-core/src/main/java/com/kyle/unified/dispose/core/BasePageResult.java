package com.kyle.unified.dispose.core;

import java.util.List;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 返回分页结果接口定义
 */
public interface BasePageResult<T> {
    /**
     * 记录该分页结果的条数
     * @param total 分页结果的记录条数
     */
    void setTotal(Long total);

    /**
     * 返回结果编码
     * @param code 自定义结果编码
     */
    void setCode(Integer code);

    /**
     * 返回结果描述
     * @param msg 自定义结果描述
     */
    void setMsg(String msg);

    /**
     * 返回结果实体类
     * @param data 自定义结果实体类
     */
    void setData(List<T> data);
}
