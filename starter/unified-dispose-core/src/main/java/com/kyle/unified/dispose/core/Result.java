package com.kyle.unified.dispose.core;

import lombok.Data;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 默认实现的结果数据结构
 */
@Data
public class Result<T> implements BaseResult<T> {
    private Integer code;

    private String msg;

    private T data;
}
