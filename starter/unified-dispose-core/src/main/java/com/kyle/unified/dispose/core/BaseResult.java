package com.kyle.unified.dispose.core;

/**
 * @author xb.zou
 * @date 2021/6/26
 * @option 返回结果接口定义
 * 该接口只定义结果的数据结构应实现方法，并规定类型，但并不限制这些属性的代表性
 * eg，你可以定义code=200时是错误，只要你的系统对于结果的业务定义合理即可。
 */
public interface BaseResult<T> {
    /**
     * 返回结果编码
     * @param code 自定义结果编码
     */
    void setCode(Integer code);

    /**
     * 返回结果描述
     * @param msg 自定义结果描述
     */
    void setMsg(String msg);

    /**
     * 返回结果实体类
     * @param data 自定义结果实体类
     */
    void setData(T data);
}
