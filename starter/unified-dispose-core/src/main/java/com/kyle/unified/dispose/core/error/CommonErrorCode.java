package com.kyle.unified.dispose.core.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author xb.zou
 * @date 2021/7/6
 * @option
 */
@AllArgsConstructor
public enum CommonErrorCode implements BaseErrorCode {
    /**
     * 通用的错误信息码
     */
    _SYSTEM_ERROR(500, "服务器开小差，请稍后再试"),
    _REQUEST_ERROR(400, "请求参数错误"),
    _REQUEST_NOT_FOUND(404, "资源未找到"),
    _METHOD_NOT_ALLOW(405, "请求方法错误"),
    _UNSUPPORTED_MEDIA_TYPE(415, "不支持该Media-type"),
    _REQUEST_READ_FAIL(526, "http消息读取失败"),
    _CONSTRAINT_VIOLATION_ERROR(526, "违法约束条件"),
    _PARAM_INVALID_ERROR(540, "参数错误"),
    _SENTINEL_FALLBACK_ERROR(509, "sentinel熔断或降级"),
    _REQUEST_REPEAT_SUBMIT(410, "该请求不允许重复提交"),
    _REQUEST_ID_NOT_FOUND(411, "请求参数中找不到ID"),
    _REQUEST_DATA_NOT_FOUND(412, "数据库中没有找到该数据"),


    /**
     * 工具类错误信息
     */
    _VELOCITY_INIT_ERROR(9001, "Velocity自动生成代码错误"),
    _VELOCITY_PACKAGE_NAME_NOT_NULL(9002, "自动生成代码时必须设置包名"),
    _VELOCITY_DOMAIN_NAME_NOT_NULL(9003, "自动生成代码时必须设置实体类类名"),
    _FILE_IS_DIRECTORY(9004, "该File为目录"),
    _FILE_CAN_NOT_WRITE(9005, "该File不可写"),
    _FILE_PATH_CAN_NOT_CREATED(9006, "该目录不能被创建"),
    _VALID_PARAM_ERROR(9007, "参数校验失败"),
    _ID_TO_LARGE(9008, "ID过大"),
    _CLOCK_BACK(9009, "时钟回摆异常"),

    /**
     * 测试错误
     */
    _TEST_EXCEPTION(-1, "测试错误");

    @Getter
    private final Integer code;

    @Getter
    @Setter
    private String message;
}
