package com.kyle.authcenter.config;

import com.kyle.authcenter.feign.UserFeignClient;
import com.kyle.common.tools.asserts.AssertUtil;
import com.kyle.user.UserCommonErrorCode;
import com.kyle.user.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class AuthUserDetailsService implements UserDetailsService {

    @Resource
    private UserFeignClient userFeignClient;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        // 获取缓存中用户信息
        UserDTO userInfo = userFeignClient.getUserInfo(userName);
        AssertUtil.nonNull(userInfo, UserCommonErrorCode._USER_NOT_FOUND);

        // 1、 User <-> Role <-> Permission 权限控制
        List<SimpleGrantedAuthority> grantedAuthorities = userInfo.getRoles().stream()
                .flatMap(role -> role.getPermissions().stream())
                .map(permission -> new SimpleGrantedAuthority(permission.getPermissionFlag()))
                .collect(Collectors.toList());

        return new User(
                userInfo.getName(),
                userInfo.getPassword(),
                grantedAuthorities
        );
    }
}
