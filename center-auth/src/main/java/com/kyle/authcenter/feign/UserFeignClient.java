package com.kyle.authcenter.feign;

import com.kyle.user.service.UserOutService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author zouxiaobang
 * @date 2021/6/22
 */
@FeignClient(name = "user-center")
public interface UserFeignClient extends UserOutService {
}
