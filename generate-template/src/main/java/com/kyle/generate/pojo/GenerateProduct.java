package com.kyle.generate.pojo;

import lombok.Data;

import java.io.File;
import java.util.List;

/**
 * @author xb.zou
 * @date 2020/9/27
 * @option
 */
@Data
public class GenerateProduct {
    /**
     * 所有的文件
     */
    private List<File> files;
    /**
     * 所有的文件，包含目录
     */
    private List<File> allFiles;
}
