package com.kyle.generate.pojo;

import lombok.Data;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
@Data
public class FieldData {
    /**
     * 时间类型
     */
    public static final String TYPE_DATE = "Date";
    /**
     * 高精度计算类型
     */
    public static final String TYPE_BIGDECIMAL = "BigDecimal";

    /**
     * 属性名称
     */
    private String name;
    /**
     * 属性类型
     */
    private Class<?> type;
    /**
     * 类型名称
     */
    private String typeName;
    /**
     * 属性的值
     */
    private Object value;
    /**
     * 属性在数据库中的名称
     */
    private String columnName;

    /**
     * 是否为父类的属性
     */
    private boolean isSupperField;
}
