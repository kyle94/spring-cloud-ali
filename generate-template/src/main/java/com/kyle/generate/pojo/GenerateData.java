package com.kyle.generate.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
@Data
public class GenerateData {
    private String code;
    private Global global;

    @Data
    public static class Global {
        private String author;
        private String packageName;
        private String domainClassPkg;
        private String domainClassName;
        private String urlPrefix;

        private List<FieldData> fieldDataList;
    }
}
