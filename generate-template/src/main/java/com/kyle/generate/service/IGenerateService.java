package com.kyle.generate.service;

import com.kyle.generate.pojo.GenerateData;
import com.kyle.generate.pojo.GenerateProduct;

import java.util.Map;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
public interface IGenerateService {
    /**
     * 预览生成的代码
     */
    Map<String, GenerateData> previewCode(String author, String packageName, String domainClassPkg, String domainClassName);

    /**
     * 生成代码 -- 生成到zip中
     */
    GenerateProduct generateCode(String author, String packageName, String domainClassPkg, String domainClassName, String moduleName);

    /**
     * 生成代码 -- 生成到zip中
     */
    byte[] generateCodeToZip(String author, String packageName, String domainClassPkg, String domainClassName, String moduleName);
}
