package com.kyle.generate.service.impl;

import com.kyle.common.tools.asserts.AssertUtil;
import com.kyle.common.tools.file.FileUtil;
import com.kyle.common.tools.file.ZipUtil;
import com.kyle.common.tools.str.StringUtil;
import com.kyle.generate.config.GeneratorConfig;
import com.kyle.generate.pojo.FieldData;
import com.kyle.generate.pojo.GenerateData;
import com.kyle.generate.pojo.GenerateProduct;
import com.kyle.generate.service.IGenerateService;
import com.kyle.generate.tools.GenerateDataUtil;
import com.kyle.generate.tools.VelocityHelper;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
@Slf4j
@Service
public class GenerateServiceImpl implements IGenerateService {
    private static final String DEFAULT_MI_DOMAIN_PKG = ".pojo.entities";

    @Override
    public Map<String, GenerateData> previewCode(String author, String packageName, String domainClassPkg, String domainClassName) {
        // 判断入参
        author = StringUtils.isEmpty(author) ? (StringUtils.isEmpty(GeneratorConfig.author) ?
                "kyle" : GeneratorConfig.author) :author;

        packageName = StringUtils.isEmpty(packageName) ? GeneratorConfig.packageName : packageName;
        AssertUtil.nonBlank(packageName, CommonErrorCode._VELOCITY_PACKAGE_NAME_NOT_NULL);

        domainClassPkg = StringUtils.isEmpty(domainClassPkg) ?
                (StringUtils.isEmpty(GeneratorConfig.domainClassPkg) ?
                        packageName + DEFAULT_MI_DOMAIN_PKG : GeneratorConfig.domainClassPkg) :domainClassPkg;

        domainClassName = StringUtils.isEmpty(domainClassName) ? GeneratorConfig.domainClassName : domainClassName;
        AssertUtil.nonNull(domainClassName, CommonErrorCode._VELOCITY_DOMAIN_NAME_NOT_NULL);

        try {
            VelocityHelper.initialize();

            List<FieldData> fieldDataList = GenerateDataUtil.parseGenerateData(domainClassPkg, domainClassName);

            // 封装VelocityContext，对应模板中的字段
            GenerateData.Global global = new GenerateData.Global();
            global.setAuthor(author);
            global.setDomainClassName(domainClassName);
            global.setDomainClassPkg(domainClassPkg);
            global.setPackageName(packageName);
            global.setUrlPrefix(StringUtil.humpToLine(domainClassName));
            global.setFieldDataList(fieldDataList);
            VelocityContext context = VelocityHelper.prepareContext(global);

            // 获取所有的Template
            List<String> templateNames = VelocityHelper.getAllTemplateNames();
            Map<String, GenerateData> result = new HashMap<>(templateNames.size());

            for (String templateName : templateNames) {
                // 开始渲染
                StringWriter stringWriter = new StringWriter();
                Template template = Velocity.getTemplate(templateName);
                template.merge(context, stringWriter);

                GenerateData data = new GenerateData();
                data.setGlobal(global);
                data.setCode(stringWriter.toString());
                result.put(templateName, data);
            }

            return result;
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage());
            throw new ServiceException(CommonErrorCode._VELOCITY_INIT_ERROR);
        }
    }

    @Override
    public GenerateProduct generateCode(String author, String packageName, String domainClassPkg, String domainClassName, String moduleName) {
        // domain生成的多个模板
        Map<String, GenerateData> codeMap = previewCode(author, packageName, domainClassPkg, domainClassName);

        // 所有的文件
        List<File> files = new ArrayList<>();
        // 所有的文件，包含目录
        List<File> allFiles = new ArrayList<>();

        for (Map.Entry<String, GenerateData> entry : codeMap.entrySet()) {
            String templateName = entry.getKey();
            GenerateData generateData = entry.getValue();

            // 将内容写入到文件中
            String codePath = GenerateDataUtil.getCodePath(
                    templateName,
                    generateData.getGlobal().getPackageName(),
                    generateData.getGlobal().getDomainClassName(),
                    moduleName);

            File file = new File(codePath);
            files.add(file);
            allFiles.addAll(FileUtil.writeToFile(file, generateData.getCode(), StandardCharsets.UTF_8.name()));
        }

        GenerateProduct product = new GenerateProduct();
        product.setFiles(files);
        product.setAllFiles(allFiles);

        return product;
    }

    @Override
    public byte[] generateCodeToZip(String author, String packageName, String domainClassPkg, String domainClassName, String moduleName) {
        GenerateProduct product = generateCode(author, packageName, domainClassPkg, domainClassName, moduleName);

        // 压缩文件成zip
        byte[] data = ZipUtil.writeToZip(product.getFiles());

        // 删除的本地文件
        FileUtil.deleteFiles(product.getAllFiles());

        return data;
    }
}
