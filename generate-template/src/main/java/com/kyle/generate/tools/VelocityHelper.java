package com.kyle.generate.tools;

import com.kyle.common.tools.type.SimpleTypeUtil;
import com.kyle.generate.pojo.FieldData;
import com.kyle.generate.pojo.GenerateData;
import com.kyle.unified.dispose.core.error.CommonErrorCode;
import com.kyle.unified.dispose.core.error.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.util.StringUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option Velocity 工具类
 */
@Slf4j
public class VelocityHelper {
    public static final String PROJECT_PATH = "main" + File.separator + "java";

    private static final String VM_FILE_PREFIX = "vm/java/";
    private static final String VM_DTO = "dto.java.vm";
    private static final String VM_REQUEST = "create_request.java.vm";
    private static final String VM_UPDATE_REQUEST = "update_request.java.vm";
    private static final String VM_RESPONSE = "response.java.vm";
    private static final String VM_REPOSITORY = "dao.java.vm";
    private static final String VM_SERVICE = "service.java.vm";
    private static final String VM_SERVICE_IMPL = "service_impl.java.vm";
    private static final String VM_CONTROLLER = "vcontroller.java.vm";


    /**
     * 初始化方法，对vm目录进行初始化
     */
    public static void initialize() {
        Properties properties = new Properties();

        try {
            // 加载classpath对应的目录下文件
            properties.setProperty("resource.loader", "class");
            properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            // 定义字符集
            properties.setProperty(Velocity.OUTPUT_ENCODING, StandardCharsets.UTF_8.displayName());
            properties.setProperty(Velocity.INPUT_ENCODING, StandardCharsets.UTF_8.displayName());

            Velocity.init(properties);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ServiceException(CommonErrorCode._VELOCITY_INIT_ERROR);
        }
    }

    /**
     * 准备/构建VelocityContext
     */
    public static VelocityContext prepareContext(GenerateData.Global global) {
        VelocityContext context = new VelocityContext();

        context.put("packageName", global.getPackageName());
        context.put("domainClassPkg", global.getDomainClassPkg());
        context.put("importList", lookupImports(global.getFieldDataList()));
        context.put("author", global.getAuthor());
        context.put("date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        context.put("domainClassName", global.getDomainClassName());
        context.put("fields", global.getFieldDataList());
        context.put("urlPrefix", global.getUrlPrefix());
        context.put("domainParamName", transToParamName(global.getDomainClassName()));


        return context;
    }

    /**
     * 转换为属性名格式
     * TestData ==> testData
     */
    private static String transToParamName(String domainClassName) {
        if (!StringUtils.isEmpty(domainClassName) && domainClassName.length() > 0) {
            String first = domainClassName.substring(0, 1);
            String after = domainClassName.substring(1);
            return first.toLowerCase() + after;
        }

        return "";
    }

    /**
     * 获取所有的模板名称
     * 采用硬编码
     */
    public static List<String> getAllTemplateNames() {
        List<String> templateNames = new ArrayList<>();
        templateNames.add(VM_FILE_PREFIX + VM_DTO);
        templateNames.add(VM_FILE_PREFIX + VM_REQUEST);
        templateNames.add(VM_FILE_PREFIX + VM_RESPONSE);
        templateNames.add(VM_FILE_PREFIX + VM_REPOSITORY);
        templateNames.add(VM_FILE_PREFIX + VM_SERVICE);
        templateNames.add(VM_FILE_PREFIX + VM_SERVICE_IMPL);
        templateNames.add(VM_FILE_PREFIX + VM_CONTROLLER);
        templateNames.add(VM_FILE_PREFIX + VM_UPDATE_REQUEST);

        return templateNames;
    }

    /**
     * 查询实体类属性所需要的依赖。
     */
    private static Set<String> lookupImports(List<FieldData> fieldDataList) {
        Set<String> importList = new HashSet<>();

        for (FieldData fieldData : fieldDataList) {
            if (!fieldData.isSupperField() && FieldData.TYPE_DATE.equals(fieldData.getType().getSimpleName())) {
                importList.add("java.util.Date");
            } else if (!fieldData.isSupperField() && FieldData.TYPE_BIGDECIMAL.equals(fieldData.getType().getSimpleName())) {
                importList.add("java.math.BigDecimal");
            } else if (!fieldData.isSupperField() && !SimpleTypeUtil.isSimpleType(fieldData.getType())) {
                importList.add(fieldData.getType().getName());
            }
        }

        return importList;
    }

    public static String getFileName(String templateName, String packageName, String domainClassName) {
        String path = PROJECT_PATH + File.separator + packageName.replace(".", File.separator);

        if (templateName.contains(VM_DTO)) {
            return path +
                    File.separator + "pojo" +
                    File.separator + "dto" +
                    File.separator + domainClassName + "DTO.java";
        }

        if (templateName.contains(VM_REQUEST)) {
            return path +
                    File.separator + "pojo" +
                    File.separator + "request" +
                    File.separator + "Create" +domainClassName + "Request.java";
        }

        if (templateName.contains(VM_UPDATE_REQUEST)) {
            return path +
                    File.separator + "pojo" +
                    File.separator + "request" +
                    File.separator + "Update" +domainClassName + "Request.java";
        }

        if (templateName.contains(VM_RESPONSE)) {
            return path +
                    File.separator + "pojo" +
                    File.separator + "response" +
                    File.separator + domainClassName + "Response.java";
        }

        if (templateName.contains(VM_SERVICE)) {
            return path +
                    File.separator + "service" +
                    File.separator + "I" + domainClassName + "Service.java";
        }

        if (templateName.contains(VM_SERVICE_IMPL)) {
            return path +
                    File.separator + "service" +
                    File.separator + "impl" +
                    File.separator + domainClassName + "ServiceImpl.java";
        }

        if (templateName.contains(VM_REPOSITORY)) {
            return path +
                    File.separator + "pojo" +
                    File.separator + "repositories" +
                    File.separator + domainClassName + "Repository.java";
        }

        if (templateName.contains(VM_CONTROLLER)) {
            return path +
                    File.separator + "controller" +
                    File.separator + domainClassName + "Controller.java";
        }

        return "";
    }
}
