package com.kyle.generate.tools;

import com.kyle.generate.pojo.FieldData;
import lombok.Data;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option 自动生成的数据处理类
 */
public class GenerateDataUtil {
    /**
     * 解析实体类中的所有属性
     */
    public static List<FieldData> parseGenerateData(String domainClassPkg, String domainClassName) throws ClassNotFoundException {
        Assert.notNull(domainClassPkg, "实体数据包名不能为空");
        Assert.notNull(domainClassName, "实体数据类名不能为空");

        // 实体数据类的完整名称
        String domainClassCompletedName = domainClassPkg + "." + domainClassName;

        final List<FieldData> fieldDataList = new ArrayList<>();

        try {
            boolean isSupper = false;
            // 获取本身和父级对象
            for (Class<?> className = Class.forName(domainClassCompletedName); className != Object.class; className = className.getSuperclass()) {
                // 获取该类所有私有字段
                Field[] fields = className.getDeclaredFields();

                for (Field field : fields) {
                    Object instance = className.newInstance();
                    FieldData fieldData = new FieldData();

                    field.setAccessible(true);
                    // 如果已经存在，表示子类已经覆盖了父类的属性，采用子类的属性
                    if (isExists(fieldDataList, field.getName())) {
                        continue;
                    }

                    Column columnAnnotation = field.getAnnotation(Column.class);

                    fieldData.setName(field.getName());
                    fieldData.setType(field.getType());
                    fieldData.setTypeName(field.getType().getSimpleName());
                    fieldData.setValue(field.get(instance));
                    fieldData.setColumnName(columnAnnotation != null && !StringUtils.isEmpty(columnAnnotation.name()) ?
                             columnAnnotation.name() : field.getName());
                    fieldData.setSupperField(isSupper);

                    fieldDataList.add(fieldData);
                }

                isSupper = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fieldDataList;
    }

    private static boolean isExists(List<FieldData> dataList, String fieldName) {
        return dataList.stream().anyMatch(fieldData -> fieldData.getName().equals(fieldName));
    }

    /**
     * 获取该模板生成的code所在目录
     */
    public static String getCodePath(String templateName, String packageName, String domainClassName, String moduleName) {
        System.getProperty("user.dir");
        if (StringUtils.isEmpty(moduleName)) {
            return System.getProperty("user.dir") + File.separator + "src" + File.separator +
                    VelocityHelper.getFileName(templateName, packageName, domainClassName);
        }
        return System.getProperty("user.dir") + File.separator + moduleName + File.separator + "src" + File.separator +
                VelocityHelper.getFileName(templateName, packageName, domainClassName);
    }
}
