package com.kyle.generate.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author xb.zou
 * @date 2020/9/26
 * @option
 */
@Component
@ConfigurationProperties(prefix = "generator")
@PropertySource(value = { "classpath:generator.yml"} )
public class GeneratorConfig {
    public static String author;
    public static String packageName;
    public static String domainClassName;
    public static String domainClassPkg;

    public String getAuthor() {
        return author;
    }

    @Value("${author}")
    public void setAuthor(String author) {
        GeneratorConfig.author = author;
    }

    public String getPackageName() {
        return packageName;
    }

    @Value("${packageName}")
    public void setPackageName(String packageName) {
        GeneratorConfig.packageName = packageName;
    }

    public String getDomainClassName() {
        return domainClassName;
    }

    @Value("${domainClassName}")
    public void setDomainClassName(String domainClassName) {
        GeneratorConfig.domainClassName = domainClassName;
    }

    public String getDomainClassPkg() {
        return domainClassPkg;
    }

    @Value("${domainClassPkg}")
    public void setDomainClassPkg(String domainClassPkg) {
        GeneratorConfig.domainClassPkg = domainClassPkg;
    }
}
